<?php
$__subtitle = array(
	"Login" => "Welcome back to after|mirror!",
	"Mango" => "A small, hand-picked manga collection.",
	"Gallery" => "View pretty pictures and more.",
	"Urusai" => "Kawaii nekomimis, dude.",
	"Tokyo" => "For all your music needs.",
	"Kazoku" => "Watch anime &mdash; with friends!",
	"Credits" => "This site wouldn't be possible without you guys.",
	"Uptime" => "Is it down or just me?",
	"Mail" => "Faster than sending mail via pigeon.",
	"Account" => "Hello world.",
	"Settings" => "Update your preferences.",
	"Library" => "Because the books are better than the films.",
	"Atelier" => "Simple photo editing.",
	"Notify" => "Notifications",
	"default" => "&mdash; after|mirror"
);
$__title_rw = array(
	"UrusaiPlayer" => "Urusai",
	"UrusaiRec" => "Urusai",
	"UrusaiLatte" => "Urusai"
);
foreach ($__title_rw as $k => $v) {
	$__subtitle[$k] = $__subtitle[$v];
}
?>