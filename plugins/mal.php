<?php
function mal_get_contents($url) {
	$urlhash = sha1($url);
	if (!is_dir("data/mal_cache")) mkdir("data/mal_cache");
	if (file_exists("data/mal_cache/{$urlhash}")) {
		if (filemtime("data/mal_cache/{$urlhash}") + (60 * 60 * 6) < time()) {
			// older than today
			unlink("data/mal_cache/{$urlhash}");
		}
		else {
			return file_get_contents("data/mal_cache/{$urlhash}");
		}
	}
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$headers = array();
	$headers[] = "pragma: no-cache";
	$headers[] = "accept-encoding: deflate, sdch";
	$headers[] = "accept-language: en-US,en;q=0.8,da;q=0.6";
	$headers[] = "upgrade-insecure-requests: 1";
	$headers[] = "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36";
	$headers[] = "accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
	$headers[] = "cache-control: no-cache";
	$headers[] = "Cookie: MALHLOGSESSID=b6e87df11720a01450b54194c6fb60e2; MALSESSIONID=nhan8h0p2qvo90cb6koa8ouvt7; is_logged_in=1";

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	curl_close($ch);

	file_put_contents("data/mal_cache/{$urlhash}", $result);
	return $result;
}
function mal_search_query($query) {
	$url = "http://myanimelist.net/anime.php?q={$query}&type=0&score=0&status=0&p=0&r=0&sm=0&sd=0&sy=0&em=0&ed=0&ey=0&c%5B%5D=a&c%5B%5D=b&c%5B%5D=c&c%5B%5D=f&gx=0";
	
	$dom = new DOMDocument();
	libxml_use_internal_errors(true);
	$dom->loadHTML(mal_get_contents($url));
	$xpath = new DOMXPath($dom);

	$links = $xpath->query("//div[@class='picSurround']/a");
	//var_dump($links);
	$return = array();
	foreach ($links as $link) {
		//var_dump($link);
		$href = $link->getAttribute("href");
		$text = $link->childNodes->item(1)->getAttribute("alt");
		$return[] = array("text" => $text, "href" => $href);
	}
	return $return;
}
function mal_parse_listing($url) {
	$dom = new DOMDocument();
	libxml_use_internal_errors(true);
	$dom->loadHTML(mal_get_contents($url));
	$xpath = new DOMXPath($dom);

	//$links = $xpath->query("//div[@class='picSurround']/a");
	//var_dump($links);
	$return = array(
		"title" => "",
		"poster" => "",
		"synonyms" => "",
		"type" => "",
		"episodes" => "",
		"status" => "",
		"airdate" => "",
		"premier" => "",
		"broadcast" => "",
		"producers" => "",
		"licensors" => "",
		"studios" => "",
		"source" => "",
		"genres" => "",
		"duration" => "",
		"rating" => "",
		"score" => "",
		"ranking" => "",
		"popularity" => "",
		"members" => "",
		"favorites" => ""
	);
	
	$return["title"] = $xpath->query("//span[@itemprop='name']")->item(0)->nodeValue;
	$return["poster"] = $xpath->query("//img[@itemprop='image']")->item(0)->getAttribute("src");
	
	foreach($xpath->query("//div[@class='js-scrollfix-bottom']/div") as $node) {
		$val = trim($node->nodeValue);
		if (strlen($val) === 0) continue;
		if (strpos($val, ":") <= 0) continue;
		
		$type = strtolower(substr($val, 0, strpos($val, ":")));
		$val = trim(substr($val, strpos($val, ":") + 1));
		
		//if ($type === 'status') continue;
		
		switch ($type) {
			case "type":
				$return[$type] = $val;
			break;
			case "episodes":
				$return[$type] = $val;
			break;
			case "status":
				$return[$type] = $val;
			break;
			case "aired":
				$return["airdate"] = $val;
			break;
			case "premiered":
				$return["premier"] = $val;
			break;
			case "broadcast":
				$return[$type] = $val;
			break;
			case "producers":
				if (strpos($val, ', add some') > 0) $val = '--';
				$return[$type] = $val;
			break;
			case "licensors":
				if (strpos($val, ', add some') > 0) $val = '--';
				$return[$type] = $val;
			break;
			case "studios":
				$return[$type] = $val;
			break;
			case "source":
				$return[$type] = $val;
			break;
			case "genres":
				$return[$type] = $val;
			break;
			case "duration":
				$return[$type] = $val;
			break;
			case "rating":
				$return[$type] = $val;
			break;
			case "score":
				$val = substr($val, 0, stripos($val, ' ') - 1);
				$return[$type] = $val;
			break;
			case "ranked":
				$val = substr($val, 0, stripos($val, ' ') - 2);
				$return["ranking"] = $val;
			break;
			case "popularity":
				$return[$type] = $val;
			break;
			case "members":
				$return[$type] = $val;
			break;
			case "favorites":
				$return[$type] = $val;
			break;
		}
	}
	$return["synopsis"] = $dom->saveHTML($xpath->query("//span[@itemprop='description']")->item(0));
	
	$ops = $xpath->query("//div[@class='opnening']/span[@class='theme-song']");
	foreach ($ops as $op) {
		$return["op"][] = $op->nodeValue;
	}
	$eds = $xpath->query("//div[@class='ending']/span[@class='theme-song']");
	foreach ($eds as $ed) {
		$return["ed"][] = $ed->nodeValue;
	}
	
	// clean
	foreach ($return as $k => $v) {
		if ($k == "synopsis") continue;
		$return[$k] = htmlentities(trimspace(strtr(trim($v), array("\r" => "", "\n" => ""))));
	}
	
	// backwards compat.
	$return["air_season"] = $return["premier"];
	
	return $return;
}
function trimspace($str) {
	return preg_replace('/\s\s+/', ' ', $str);
}
?>