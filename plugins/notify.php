<?php
define("NOTIFY_GENERAL", 1);
define("NOTIFY_MAIL", 2);

class Notify {
	var $db;
	
	function __construct($mysqli) {
		$this->db = $mysqli;
	}
	
	function createNotification($user_id, $title, $content, $type, $action) {
		$stmt = $this->db->prepare("INSERT INTO notifications (uid, title, content, type, action) VALUES (?, ?, ?, ?, ?);");
		$stmt->bind_param('issis', $user_id, $title, $content, $type, $action);
		
		$stmt->execute() or die($this->db->error);
	}
	function getNotifications($user_id) {
		$stmt = $this->db->prepare("SELECT * FROM notifications WHERE uid = ?;");
		$stmt->bind_param('i', $user_id);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		
		$return = array();
		if ($res->num_rows >= 1) while ($row = $res->fetch_array()) {
			$return[$row["id"]] = $row;
		}
		knatsort($return);
		return $return;
	}
	function getNotificationCount($user_id) {
		$stmt = $this->db->prepare("SELECT * FROM notifications WHERE uid = ?;");
		$stmt->bind_param('i', $user_id);
		$stmt->execute() or die($this->db->error);
		$res = $stmt->get_result();
		return $res->num_rows;
	}
	function readNotification($notification_id) {
		$stmt = $this->db->prepare("UPDATE notifications SET read = 1 WHERE id = ?;");
		$stmt->bind_param('i', $notification_id);
		$stmt->execute() or die($this->db->error);
	}
	function deleteNotification($notification_id) {
		$stmt = $this->db->prepare("DELETE FROM notifications WHERE id = ?;");
		$stmt->bind_param('i', $notification_id);
		$stmt->execute() or die($this->db->error);
	}
}
?>