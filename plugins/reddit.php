<?php
function reddit_get_contents($url) {
	$urlhash = sha1($url);
	if (!is_dir("data/reddit_cache")) mkdir("data/reddit_cache");
	if (file_exists("data/reddit_cache/{$urlhash}")) {
		if (filemtime("data/reddit_cache/{$urlhash}") + (60 * 60 * 24) < time()) {
			// older than today
			unlink("data/reddit_cache/{$urlhash}");
		}
		else {
			return file_get_contents("data/reddit_cache/{$urlhash}");
		}
	}
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$headers = array();
	$headers[] = "accept-language: en-US,en;q=0.8,da;q=0.6";
	$headers[] = "user-agent: urusai.ninja discussion finder (urusai.ninja)";

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	curl_close($ch);

	file_put_contents("data/reddit_cache/{$urlhash}", $result);
	return $result;
}
?>