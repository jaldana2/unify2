<?php
class Library {
	var $db;
	
	function __construct($mysqli) {
		$this->db = $mysqli;
	}
	
	function addSeries($title, $author) {
		$stmt = $this->db->prepare("INSERT INTO library_collection (title, author) VALUES (?, ?);");
		$stmt->bind_param('ss', $title, $author);
		$stmt->execute() or die($this->db->error);
	}
	function getAllSeries() {
		$res = $this->db->query("SELECT * FROM library_collection;");
		$return = array();
		if ($res) while ($row = $res->fetch_array()) {
			$return[] = $row;
		}
		return $return;
	}
	function getSeries($series_id) {
		$stmt = $this->db->prepare("SELECT * FROM library_collection WHERE id = ?;");
		$stmt->bind_param('i', $series_id);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		
		if ($res) while ($row = $res->fetch_array()) {
			return $row;
		}
		return false;
	}
	function updateSeries($series_id, $title, $author) {
		$stmt = $this->db->prepare("UPDATE library_collection SET title = ?, author = ? WHERE id = ?;");
		$stmt->bind_param('ssi', $title, $author, $series_id);
		$stmt->execute() or die($this->db->error);
	}
	function addVolume($series_id, $source, $title, $volume) {
		$stmt = $this->db->prepare("INSERT INTO library_media (sid, source, title, volume) VALUES (?, ?, ?, ?);");
		$stmt->bind_param('issi', $series_id, $source, $title, $volume);
		$stmt->execute() or die($this->db->error);
	}
	function getAllVolumes($series_id) {
		$stmt = $this->db->prepare("SELECT * FROM library_media WHERE sid = ?;");
		$stmt->bind_param('i', $series_id);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		
		$return = array();
		if ($res) while ($row = $res->fetch_array()) {
			$return[] = $row;
		}
		return $return;
	}
	function getVolume($book_id) {
		$stmt = $this->db->prepare("SELECT * FROM library_media WHERE id = ?;");
		$stmt->bind_param('i', $book_id);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		
		if ($res) while ($row = $res->fetch_array()) {
			return $row;
		}
		return false;
	}
	function updateVolume($book_id, $source, $title, $volume) {
		$stmt = $this->db->prepare("UPDATE library_media SET source = ?, title = ?, volume = ? WHERE id = ?;");
		$stmt->bind_param('ssii', $source, $title, $volume, $book_id);
		$stmt->execute() or die($this->db->error);
	}
	function getPosition($user_id, $book_id) {
		$stmt = $this->db->prepare("SELECT * FROM library_position WHERE bid = ? AND uid = ?;");
		$stmt->bind_param('ii', $book_id, $user_id);
		$stmt->execute() or die($this->db->error);
		$res = $stmt->get_result();
		
		if ($res) while ($row = $res->fetch_array()) {
			return $row;
		}
		return false;
	}
	function savePosition($user_id, $book_id, $page) {
		$page++; // offset by 1
		if (!$this->getPosition($user_id, $book_id)) {
			$stmt = $this->db->prepare("INSERT INTO library_position (bid, uid, page, time) VALUES (?, ?, ?, ?);");
			$stmt->bind_param('iiii', $book_id, $user_id, $page, time());
			$stmt->execute() or die($this->db->error);
		}
		else {
			$stmt = $this->db->prepare("UPDATE library_position SET page = ?, time = ? WHERE bid = ? AND uid = ?;");
			$stmt->bind_param('iiii', $page, time(), $book_id, $user_id);
			$stmt->execute() or die($this->db->error);
		}
	}
}
?>