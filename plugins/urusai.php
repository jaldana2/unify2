<?php
define("URUSAI_ANIME_ERROR", -1);
class Urusai {
	var $db;
	
	function __construct($mysqli) {
		$this->db = $mysqli;
	}
	
	function getListing() {
		$res = $this->db->query("SELECT * FROM urusai_anime;");
		
		$return = array();
		if ($res) while ($row = $res->fetch_array()) {
			$return[$row["id"]] = $row["title"];
		}
		return $return;
	}
	function animeExists($title) {
		$stmt = $this->db->prepare("SELECT title FROM urusai_anime WHERE title = ?;");
		$stmt->bind_param('s', $title);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		if ($res->num_rows == 1) {
			// exists
			return true;
		}
		return false;
	}
	function addAnime($title, $alt_title, $air_season, $num_episodes, $genres, $ranking, $score, $synopsis, $airing, $status) {
		$stmt = $this->db->prepare("INSERT INTO urusai_anime (title, alt_title, air_season, num_episodes, genres, ranking, score, synopsis, airing, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
		$stmt->bind_param('sssisidsii', $title, $alt_title, $air_season, $num_episodes, $genres, $ranking, $score, $synopsis, $airing, $status);
		
		$stmt->execute() or die($this->db->error);
	}
	function updateAnime($id, $title, $alt_title, $air_season, $num_episodes, $genres, $ranking, $score, $synopsis, $airing, $status) {
		$stmt = $this->db->prepare("UPDATE urusai_anime SET title = ?, alt_title = ?, air_season = ?, num_episodes = ?, genres = ?, ranking = ?, score = ?, synopsis = ?, airing = ?, status = ? WHERE id = ?;");
		$stmt->bind_param('sssisidsiii', $title, $alt_title, $air_season, $num_episodes, $genres, $ranking, $score, $synopsis, $airing, $status, $id);
		
		$stmt->execute() or die($this->db->error);
	}
	function getAnimeAttribute($id, $attribute) {
		$stmt = $this->db->prepare("SELECT {$attribute} FROM urusai_anime WHERE id = ?;");
		$stmt->bind_param('i', $id);
		$stmt->execute() or die($this->db->error);
		$stmt->bind_result($res);
		$stmt->fetch();
		return $res;
	}
	function setAnimeAttribute($id, $attribute, $value, $type) {
		$stmt = $this->db->prepare("UPDATE urusai_anime SET " . $attribute . " = ? WHERE id = ?;") ;
		switch ($type) {
			case SQLITE3_INTEGER:
				$stmt->bind_param('ii', $value, $id);
			break;
			case SQLITE3_TEXT:
				$stmt->bind_param('si', $value, $id);
			break;
		}
		$stmt->execute() or die($this->db->error);
	}
	function getAnimeAttributes($id) {
		$stmt = $this->db->prepare("SELECT * FROM urusai_anime WHERE id = ?;");
		$stmt->bind_param('i', $id);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		if ($res->num_rows == 1) {
			// exists
			return $res->fetch_array();
		}
		return false;
	}
	
	function getAnimeID($title) {
		$stmt = $this->db->prepare("SELECT id FROM urusai_anime WHERE title = ?;");
		$stmt->bind_param('s', $title);
		$stmt->execute() or die($this->db->error);
		$stmt->bind_result($id);
		$stmt->fetch();
		return $id;
	}
	function getEpisodeID($anime_id, $episode_num) {
		$stmt = $this->db->prepare("SELECT id FROM urusai_episodes WHERE anime_id = ? AND absolute_episode = ?;");
		$stmt->bind_param('ii', $anime_id, $episode_num);
		$stmt->execute() or die($this->db->error);
		$stmt->bind_result($id);
		$stmt->fetch();
		return $id;
	}
	function animeEpisodeExists($anime_id, $episode_num) {
		$stmt = $this->db->prepare("SELECT episode_title FROM urusai_episodes WHERE anime_id = ? AND absolute_episode = ?;");
		$stmt->bind_param('ii', $anime_id, $episode_num);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		if ($res->num_rows >= 1) {
			// exists
			return true;
		}
		return false;
	}
	function animeEpisodeMirrorExists($episode_id, $source) {
		$stmt = $this->db->prepare("SELECT source FROM urusai_files WHERE episode_id = ? AND source = ?;");
		$stmt->bind_param('is', $episode_id, $source);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		if ($res->num_rows >= 1) {
			// exists
			return true;
		}
		return false;
	}
	function getEpisodeMirrors($episode_id) {
		$stmt = $this->db->prepare("SELECT * FROM urusai_files WHERE episode_id = ?;");
		$stmt->bind_param('i', $episode_id);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		
		$return = array();
		if ($res) while ($row = $res->fetch_array()) {
			$return[$row["id"]] = array(
				"quality" => $row["quality"],
				"source" => $row["source"]
			);
		}
		return $return;
	}
	function getEpisodeMirror($mirror_id) {
		$stmt = $this->db->prepare("SELECT * FROM urusai_files WHERE id = ?;");
		$stmt->bind_param('i', $mirror_id);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		
		if ($res) while ($row = $res->fetch_array()) {
			return $row;
		}
		return false;
	}
	function getLatestEpisodes() {
		$stmt = $this->db->prepare("SELECT * FROM urusai_episodes ORDER BY airdate DESC LIMIT 40;");
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		
		$return = array();
		if ($res->num_rows >= 1) while ($row = $res->fetch_array()) {
			$return[] = $row;
		}
		knatsort($return);
		return $return;
	}
	function getEpisodes($id) {
		$stmt = $this->db->prepare("SELECT * FROM urusai_episodes WHERE anime_id = ? ORDER BY absolute_episode DESC;");
		$stmt->bind_param('i', $id);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		
		$return = array();
		if ($res->num_rows >= 1) while ($row = $res->fetch_array()) {
			$return[$row["absolute_episode"]] = $row;
		}
		knatsort($return);
		return $return;
	}
	function getEpisode($id) {
		$stmt = $this->db->prepare("SELECT * FROM urusai_episodes WHERE id = ?;");
		$stmt->bind_param('i', $id);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		
		if ($res) while ($row = $res->fetch_array()) {
			return $row;
		}
		return false;
	}
	function addEpisode($id, $episode, $episode_title, $desc, $airdate, $s_episode, $s_season) {
		$stmt = $this->db->prepare("INSERT INTO urusai_episodes (anime_id, absolute_episode, episode_title, description, airdate, episode, season) VALUES (?, ?, ?, ?, ?, ?, ?);");
		$stmt->bind_param('iisssii', $id, $episode, $episode_title, $desc, $airdate, $s_episode, $s_season);
		
		$stmt->execute() or die($this->db->error);
	}
	function updateEpisode($episode_id, $id, $episode, $episode_title, $desc, $airdate, $s_episode, $s_season) {
		$stmt = $this->db->prepare("UPDATE urusai_episodes SET anime_id = ?, absolute_episode = ?, episode_title = ?, description = ?, airdate = ?, episode = ?, season = ? WHERE id = ?;");
		$stmt->bind_param('iisssiii', $id, $episode, $episode_title, $desc, $airdate, $episode_id, $s_episode, $s_season);
		
		$stmt->execute() or die($this->db->error);
	}
	function addEpisodeMirror($id, $source, $quality) {
		$stmt = $this->db->prepare("INSERT INTO urusai_files (episode_id, source, quality) VALUES (?, ?, ?);");
		$stmt->bind_param('iss', $id, $source, $quality);
		
		$stmt->execute() or die($this->db->error);
	}
	function updateEpisodeMirror($mirror_id, $id, $source, $quality) {
		$stmt = $this->db->prepare("UPDATE urusai_files SET episode_id = ?, source = ?, quality = ? WHERE id = ?;");
		$stmt->bind_param('issi', $id, $source, $quality, $mirror_id);
		
		$stmt->execute() or die($this->db->error);
	}
	function setEpisodeAttribute($id, $attribute, $value, $type) {
		$stmt = $this->db->prepare("UPDATE urusai_episodes SET " . $attribute . " = ? WHERE id = ?;");
		switch ($type) {
			case SQLITE3_INTEGER:
				$stmt->bind_param('ii', $value, $id);
			break;
			case SQLITE3_TEXT:
				$stmt->bind_param('si', $value, $id);
			break;
		}
		$stmt->execute() or die($this->db->error);
	}
	function deleteAnime($anime_id) {
		$stmt = $this->db->prepare("DELETE FROM urusai_anime WHERE id = ?;");
		$stmt->bind_param('i', $anime_id);
		$stmt->execute() or die($this->db->error);
	}
	function deleteEpisodes($anime_id) {
		$stmt = $this->db->prepare("DELETE FROM urusai_episodes WHERE anime_id = ?;");
		$stmt->bind_param('i', $anime_id);
		$stmt->execute() or die($this->db->error);
	}
	function deleteEpisode($episode_id) {
		$stmt = $this->db->prepare("DELETE FROM urusai_episodes WHERE id = ?;");
		$stmt->bind_param('i', $episode_id);
		$stmt->execute() or die($this->db->error);
	}
	function deleteMirrors($episode_id) {
		$stmt = $this->db->prepare("DELETE FROM urusai_files WHERE episode_id = ?;");
		$stmt->bind_param('i', $episode_id);
		$stmt->execute() or die($this->db->error);
	}
	function deleteMirror($mirror_id) {
		$stmt = $this->db->prepare("DELETE FROM urusai_files WHERE id = ?;");
		$stmt->bind_param('i', $mirror_id);
		$stmt->execute() or die($this->db->error);
	}
}
?>