<?php
class Messages {
	var $db;
	
	function __construct($mysqli) {
		$this->db = $mysqli;
	}
	
	function createMail($user_id, $recipient, $title, $content) {
		$stmt = $this->db->prepare("INSERT INTO mail (uid, rid, title, content) VALUES (?, ?, ?, ?);");
		$stmt->bind_param('iiss', $user_id, $recipient, $title, $content);
		
		$stmt->execute() or die($this->db->error);
	}
	function getMail($user_id) {
		$stmt = $this->db->prepare("SELECT * FROM mail WHERE rid = ?;");
		$stmt->bind_param('i', $user_id);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		
		$return = array();
		if ($res->num_rows >= 1) while ($row = $res->fetch_array()) {
			$return[$row["id"]] = $row;
		}
		knatsort($return);
		return $return;
	}
	function getMailSent($user_id) {
		$stmt = $this->db->prepare("SELECT * FROM mail WHERE uid = ?;");
		$stmt->bind_param('i', $user_id);
		$stmt->execute() or die($this->db->error);
		
		$res = $stmt->get_result();
		
		$return = array();
		if ($res->num_rows >= 1) while ($row = $res->fetch_array()) {
			$return[$row["id"]] = $row;
		}
		knatsort($return);
		return $return;
	}
	function getMailCount($user_id) {
		$stmt = $this->db->prepare("SELECT * FROM mail WHERE rid = ?;");
		$stmt->bind_param('i', $user_id);
		$stmt->execute() or die($this->db->error);
		$res = $stmt->get_result();
		return $res->num_rows;
	}
	function readMail($mail_id) {
		$stmt = $this->db->prepare("UPDATE mail SET read = 1 WHERE id = ?;");
		$stmt->bind_param('i', $mail_id);
		$stmt->execute() or die($this->db->error);
	}
	function deleteMail($mail_id) {
		$stmt = $this->db->prepare("DELETE FROM mail WHERE id = ?;");
		$stmt->bind_param('i', $mail_id);
		$stmt->execute() or die($this->db->error);
	}
}
?>