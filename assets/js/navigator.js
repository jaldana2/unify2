var navigatorAjax = false; // turn off page transitions for now.
var pendingURL;
var pendingTitle;
var pendingHref;

$("body").on("click", "a.content-link", function(e) {
	pendingURL = $(this).attr("href");
	pendingTitle = "a|m - " + $(this).attr("data-title");
	pendingHref = $(this).attr("href");
	if ($(this)[0].hasAttribute("ajax-href")) {
		pendingHref = $(this).attr("ajax-href");
		console.log("Using " + pendingHref + " instead.");
	}
	//$("#main-content").html($("#page-transition-container").html());
	//$("#main-content").hide();
	$("#page-transition-container").fadeIn(200);
	
	//setTimeout(delayedLoad, 1500);
	delayedLoad();
	
	e.preventDefault();
});
$("body").on("click", ".x-content-link", function(e) {
	pendingURL = $(this).attr("data-href");
	pendingTitle = "a|m - " + $(this).attr("data-title");
	pendingHref = $(this).attr("data-href");
	if ($(this)[0].hasAttribute("ajax-href")) {
		pendingHref = $(this).attr("ajax-href");
		console.log("Using " + pendingHref + " instead.");
	}
	$("#page-transition-container").fadeIn(200);
	
	delayedLoad();
	
	e.preventDefault();
});
function delayedLoad() {
	if (!navigatorAjax) {
		location.href = pendingURL; // pendingHref is for ajax calls.
	}
	else {
		var app2doHref = pendingHref;
		if (pendingHref.substring(0, 4) == "/app") {
			app2doHref = "/do" + pendingHref.substring("/app".length);
		}
		if (app2doHref == "/") { // root dir
			// root link
			$("#main-content").load(pendingHref + " #main-content .main-container-ajax", function(e) {
				$("#main-content").hide().fadeIn(400);
				$("#page-transition-container").hide();
				window.history.pushState({ "mainContent": $("#main-content").html(), "pageTitle": document.title, "pHref": pendingHref }, document.title, pendingURL);
				document.title = pendingTitle;
				$.getScript("/do/AjaxScript?req=" + pendingURL);
				uiInit();
			});
		}
		else {
			
			$.ajax({
				url: app2doHref,
				success: function(html) {
					$('#main-content').hide().html(html).fadeIn(400);
					$("#page-transition-container").hide();
					window.history.pushState({ "mainContent": $("#main-content").html(), "pageTitle": document.title, "pHref": pendingHref }, document.title, pendingURL);
					document.title = pendingTitle;
					$.getScript("/do/AjaxScript?req=" + pendingHref);
					uiInit();
				}
			});
		}
		
		/*
		$("#main-content").load(pendingHref + " #main-content .main-container-ajax", function(e) {
			window.history.pushState({ "mainContent": $("#main-content").html(), "pageTitle": document.title }, document.title, pendingURL);
			document.title = pendingTitle;
			$.getScript("/do/AjaxScript?req=" + pendingURL);
			uiInit();
		});
		*/
	}
}
$(window).on("popstate", function(e) {
	//console.log(e.originalEvent.state);
	if (e.originalEvent.state != null) {
		//console.log(e.originalEvent.state);
		console.log("popstate/back button event");
		$("#main-content").html(e.originalEvent.state.mainContent);
		document.title = e.originalEvent.state.pageTitle;
		$.getScript("/do/AjaxScript?req=" + e.originalEvent.state.pHref);
		uiInit();
	}
});
// init first page onto history stack.
$(function() {
	if (navigatorAjax) {
		if (location.hash !== '') {
			window.history.pushState({ "mainContent": $("#main-content").html(), "pageTitle": document.title, "pHref": window.location.pathname }, document.title, window.location.pathname + window.location.search + location.hash);
		}
		else {
			window.history.pushState({ "mainContent": $("#main-content").html(), "pageTitle": document.title, "pHref": window.location.pathname }, document.title, window.location.pathname + window.location.search);
		}
	}
});
//window.onbeforeunload = function() { return "Your work will be lost."; };

$(function() {
	if (Cookies.get("settingsUsePageTransition") !== undefined) {
		if (Cookies.get("settingsUsePageTransition") == "no") {
			navigatorAjax = false;
		}
	}
});