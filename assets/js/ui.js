var uiInitReload = false;
var bLazy;
function uiInit() {
	if (window.location.hostname == "urusai.ninja") {
		$(".am").removeClass("am").addClass("un").html("urusai!<b>.ninja</b>");
	}
	$('.x-profile-picture').css("background-image", "url('/user/" + x_auth_user + "/profile.jpg')");
	$('.x-profile-name').text(x_auth_user);
	$('.w-profile-picture').each(function() {
		$(this).css("background-image", "url('/user/" + $(this).attr('data-user') + "/profile.jpg')");
	});
	if (uiInitReload) {
		$(".x-profile-picture").css("background-image", "url('/user/" + x_auth_user + "/profile.jpg?_r=" + Math.random() + "')");
	}
	bLazy = new Blazy();
    $(":checkbox").labelauty();
	/*$('[title]').qtip({ position: { my: 'bottom center', at: 'top center' } });*/
	$("body").sakura();
};
setTimeout("sakuraStop()", 24500);
setTimeout("sakuraStop2()", 25000);
var sakuraStop = function() { $(".sakura").hide(500); }
var sakuraStop2 = function() { $("body").sakura("stop"); }

$('.w-profile-image-wrapper').on('DOMNodeInserted', '.w-profile-picture', function(e) {
	$(this).css("background-image", "url('/user/" + $(this).attr('data-user') + "/profile.jpg')");
});
// @http://stackoverflow.com/questions/2346011/how-do-i-scroll-to-an-element-within-an-overflowed-div
jQuery.fn.scrollTo = function(elem, speed) {
	$(this).clearQueue().animate({
		scrollTop: $(this).scrollTop() - $(this).offset().top + $(elem).offset().top 
	}, speed == undefined ? 1000 : speed); 
	return this; 
};

$(function() {
	uiInit();
});