<?php
$app = basenamex($_GET["req"]);
if (file_exists("bin.js/{$app}.js")) {
	echo "
		console.log('App: {$app}');
	";
	readfile("bin.js/{$app}.js");
}
else {
	echo "
		console.log('Unknown app: {$app}');
	";
}
?>