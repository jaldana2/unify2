<?php
requireLogin();
requireAccess($auth, AUTH_ACCESS_ADMIN);

$tokyo = new Tokyo($mysqli);
$listing = $tokyo->getListing();

if (isset($_GET["update"])) {
	foreach ($_POST as $attr => $data) {
		foreach ($data as $id => $val) {
			if ($listing[$id][$attr] != $val) {
				switch ($attr) {
					case "mediaID":
					case "title":
					case "artist":
					case "artwork":
						$tokyo->modifyMusicAttribute($listing[$id]["mediaID"], $attr, $val, "s");
					break;
					case "duration":
					case "uploader":
					case "playcount":
					case "added":
						$tokyo->modifyMusicAttribute($listing[$id]["mediaID"], $attr, $val, "i");
					break;
				}
			}
		}
	}
	echo "<h3>Changes saved.</h3>";
	$listing = $tokyo->getListing();
}

echo "
	<style type='text/css'>
		input[type='text'], td, tr { padding: 0 !important; margin: 0 !important; }
		input[type='text'] { font-size: 0.5em; }
	</style>
	<form action='/app/TokyoManager?update' method='post'>
		<table>
		<tr><th>mediaID</th><th>Title</th><th>Artist</th><th>Duration</th><th>Uploader</th><th>Playcount</th><th>Artwork</th><th>Added</th></tr>
";
foreach ($listing as $id => $blob) {
	echo "
		<tr>
			<td><input type='text' name='mediaID[{$id}]' value=\"{$blob['mediaID']}\" /></td>
			<td><input type='text' name='title[{$id}]' value=\"{$blob['title']}\" /></td>
			<td><input type='text' name='artist[{$id}]' value=\"{$blob['artist']}\" /></td>
			<td><input type='text' name='duration[{$id}]' value=\"{$blob['duration']}\" /></td>
			<td><input type='text' name='uploader[{$id}]' value=\"{$blob['uploader']}\" /></td>
			<td><input type='text' name='playcount[{$id}]' value=\"{$blob['playcount']}\" /></td>
			<td><input type='text' name='artwork[{$id}]' value=\"{$blob['artwork']}\" /></td>
			<td><input type='text' name='added[{$id}]' value=\"{$blob['added']}\" /></td>
		</tr>
	";
}
echo "
		</table>
		<input type='submit' value='Save' />
	</form>
";
?>