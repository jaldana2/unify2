<?php
requireLogin();
$mail = new Messages($mysqli);

echo "
	<p>
		<a href='/app/Mail?do=compose' class='button'>Compose</a>
	</p>
";

if (isset($_GET["do"])) {
	switch ($_GET["do"]) {
		case "compose":
			if (isSomething($_POST["msgRecipient"]) && isSomething($_POST["msgContent"])) {
				foreach ($_POST["msgRecipient"] as $rec) {
					$mail->createMail(AUTH_UID, $rec, $_POST["msgTitle"], $_POST["msgContent"]);
					$notify->createNotification($rec, "New mail", $_POST["msgTitle"], NOTIFY_MAIL, "/app/Mail");
				}
				echo "
				<div class='callout callout-success'>
					<h4>Success!</h4>
					<p>Your message has been sent.</p>
				</div>
				";
			}
			echo "
					<h1>New Message</h1>
					<form role='form' action='/app/Mail?do=compose' method='post'>
						<div class='form-group'>
							<label for='msgRecipient'>To</label>
							<select name='msgRecipient[]' id='msgRecipient' class='form-control' multiple='multiple'>
			";
			$users = $auth->getAllUsers();
			natsort($users);
			foreach ($users as $user) {
				$uid = $auth->getUserID($user);
				if (isset($_GET["reply"]) && $uid == $_GET["reply"]) {
					echo "<option value='{$uid}' selected>{$user}</option>";
				}
				else {
					echo "<option value='{$uid}'>{$user}</option>";
				}
			}
			echo "
							</select>
						</div>
						<div class='form-group'>
							<label for='msgTitle'>Title</label>
							<input type='text' name='msgTitle' id='msgTitle' class='form-control' />
						</div>
						<div class='form-group'>
							<label for='msgContent'>Message</label>
							<textarea name='msgContent' id='msgContent' class='form-control' rows='3'></textarea>
						</div>
						<div class='form-group'>
							<input type='submit' value='Send' class='btn btn-primary form-control' />
						</div>
					</form>
				</div>
			";
		break;
		case "read":
			$messages = $mail->getMail(AUTH_UID);
			$message = $messages[$_GET["id"]];
			
			$sender = $auth->getUsername($message["uid"]);
			echo "
				<div class='box'>
					<div class='box-header with-border'>
						<h2>{$message['title']}</h2>
						<h3 class='box-title'>From: {$sender}</h3>
					</div>
					<div class='box-body'>
						<p>{$message['content']}</p>
					</div>
					<div class='box-footer'>
						<a href='/app/Mail?delete={$_GET['id']}' class='button small btn-danger content-link' data-title='Mail'>Delete</a>
						<a href='/app/Mail?do=compose&reply={$message['uid']}' class='button small btn-info content-link' data-title='Mail: Reply'>Reply</a>
					</div>
				</div>
			";
		break;
	}
}
else {
	echo "
		<h3 class='major'>Inbox</h3>
		<table class='table table-bordered'>
			<tr>
				<th style='width: 64px;'></th>
				<th style='width: 120px;'>Sender</th>
				<th>Title</th>
			</tr>
	";
	$messages = $mail->getMail(AUTH_UID);
	if (isset($_GET["delete"]) && isset($messages[$_GET["delete"]])) {
		$mail->deleteMail($_GET["delete"]);
		//$messages = pushValueFromArray($_GET["delete"], $messages, true);
		unset($messages[$_GET["delete"]]);
	}
	if (is_array($messages) && count($messages) >= 1) foreach ($messages as $id => $blob) {
		$sender = $auth->getUsername($blob["uid"]);
		echo "<tr><td><a href='/app/Mail?delete={$id}'><i class='fa fa-close' style='color: red;'></i></a></td><td>{$sender}</td><td><a href='/app/Mail?do=read&id={$id}'>{$blob['title']}</a></td></tr>";
	}
	else {
		echo "<tr><td colspan='3'>No messages here.</td></tr>";
	}
	echo "
		</table>
	";
}
?>