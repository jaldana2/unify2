<?php
enforceLogin();
$tokyo = new Tokyo($mysqli);

if (isset($_POST["ytid"]) && strlen($_POST["ytid"]) == 11) {
	if (substr($_POST["ytid"], 0, 1) !== "=" && !file_exists("data/tokyo/{$_POST['ytid']}.mp3") && !$tokyo->getMusic($_POST["ytid"])) {
		$root = substr(__DIR__, 0, strripos(__DIR__, "/"));
		$exec = sprintf("youtube-dl -4 --max-filesize 50m --no-playlist -x --audio-format mp3 -o %s %s", escapeshellarg($root . "/data/tokyo/%(id)s.%(ext)s"), "'https://www.youtube.com/watch?v={$_POST['ytid']}'");
		exec($exec);
		if (file_exists("data/tokyo/{$_POST['ytid']}.mp3")) {
			$exec2 = sprintf("mp3gain -r -c %s", escapeshellarg($root . "/data/tokyo/{$_POST['ytid']}.mp3"));
			exec($exec2);
			$mp3 = new MP3File("data/tokyo/{$_POST['ytid']}.mp3");
			$duration = (int) $mp3->getDuration();
			$tokyo->addMusic($_POST["ytid"], $_POST["title"], $_POST["artist"], $_POST["tags"], $duration, AUTH_UID, "//i.ytimg.com/vi/" . $_POST["ytid"] . "/0.jpg");
			echo "Upload successful.";
		}
		else {
			echo "Could not retrieve file.";
		}
	}
	elseif (substr($_POST["ytid"], 0, 1) == "=" && file_exists("data/tokyo/{$_POST['ytid']}.mp3") && !$tokyo->getMusic($_POST["ytid"])) {
		// manual
		$exec2 = sprintf("mp3gain -r -c %s", escapeshellarg($root . "/data/tokyo/{$_POST['ytid']}.mp3"));
		exec($exec2);
		$mp3 = new MP3File("data/tokyo/{$_POST['ytid']}.mp3");
		$duration = (int) $mp3->getDuration();
		$tokyo->addMusic($_POST["ytid"], $_POST["title"], $_POST["artist"], $_POST["tags"], $duration, AUTH_UID, "");
		echo "Upload successful (manual).";
	}
	else {
		echo "File exists.";
	}
}
elseif (isset($_POST["playSong"])) {
	$blob = $tokyo->getMusic($_POST["playSong"]);
	$tokyo->modifyMusicAttribute($_POST["playSong"], "playcount", $blob["playcount"] + 1, "i");
	echo "Updated.";
}
else {
	echo "Bad request.";
}
?>