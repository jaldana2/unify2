<?php
enforceLogin();

if (isset($_GET["ax"])) {
	setcookie("auth", $_GET["ax"], time() + (3600 * 24));
}
$qs = base64_decode($_GET["qs"]);
echo "
<h1>welcome back, " . AUTH_USER . "</h1>
<p>Returning you to your previous page... <span class='fa fa-spinner fa-spin'></span></p>
<br />
<a href='{$qs}' class='btn btn-info'>Click me if you are not redirected within a few seconds</a>
<script>
	setTimeout(\"location.href = '{$qs}'\", 2500);
</script>
";
?>