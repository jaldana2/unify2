<?php
requireLogin();

if (isset($_GET["do"])) {
	switch ($_GET["do"]) {
		case "profile-image-upload":
			if (!empty($_FILES)) {
				$tmp_name = $_FILES["file"]["tmp_name"];
				$file = AUTH_USER . ".jpg";
				$target = "data/account/profile/{$file}";
				if (file_exists($target)) {
					unlink($target);
					$auth->modPoints(AUTH_USER, 200);
				}
				else {
					$auth->modPoints(AUTH_USER, 2000);
				}
				iTF($tmp_name, $target, 640, 92);
			}
		break;
	}
	echo "
	<div class='container'>
		<p>Please wait... <span class='fa fa-spinner fa-spin'></span></p>
		<br />
		<a href='?app=Account' class='btn btn-info'>Click me if you are not redirected within a few seconds</a>
	</div>
	<script>
		function refresher() {
			window.location = '?app=Account&refresh';	
		}
		setTimeout('refresher()', 2500);
	</script>
	";
}
else {
	echo "
	<div class='text-center'>
		<div class='profile-picture-circle x-profile-picture'>
			<span class='profile-picture-edit-btn' id='profile-image-upload-btn'><span class='fa fa-pencil'></span> edit</span>
			<progress id='profile-picture-file-upl-progress' value='0' max='1'></progress>
		</div>
		<h2 class='profile-title x-profile-name'></h2>
	</div>
	<!--<a href='/app/UrusaiRec' class='button small btn-primary'>My Anime Recommendations</a>-->
	<a href='/app/Settings' class='button small btn-primary'>Settings</a>
	<div style='display: none;'>
		<form action='/app/Account?do=profile-image-upload' method='post' enctype='multipart/form-data' id='profile-picture-file-upl-form'>
			<input type='file' name='file' id='profile-picture-file-upl'>
		</form>
	</div>
	";	
}
?>