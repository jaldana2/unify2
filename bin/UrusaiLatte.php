<?php
requireLogin();

$json = json_decode(file_get_contents("https://owl.aftermirror.com/json.php"), true);
$col = array();
$colt = array();
foreach ($json as $file) {
	$bn = trim(substr(preg_replace('#\s*\[.+\]\s*#U', ' ', strtr($file["file"], array("_Ep" => " - ", "_" => " "))), 0, -4), " .");
	if (substr($bn, -3) == "480") {
		$col[trim(substr($bn, 0, -3))]["SD"] = "https://owl.aftermirror.com/" . $file["src"];
	}
	else {
		$col[$bn]["HD"] = "https://owl.aftermirror.com/" . $file["src"];
	}
	if (!isset($colt[$bn]) || $colt[$bn] < $file["time"]) $colt[$bn] = $file["time"];
}
knatsort($col);

echo "<h3><a href='/app/Kazoku'>Click here to watch stuff with others!</a></h3>";

echo "<table>";
foreach ($col as $bn => $blob) {
	$color = "bfbfbf";
	$timedelta = abs(time() - $colt[$bn]);
	if ($timedelta <= 60 * 30 * 24) $color = "36d456; font-weight: bold";
	$time = time_since($timedelta);
	echo "<tr><td><b>{$bn}</b><br/><span style='color: #{$color}; font-size: 0.8em;'>{$time} ago</span></td><td style='vertical-align: middle; text-align: center;'>";
	if (isset($blob["HD"])) {
		echo "<a href='{$blob['HD']}' class='label label-success'>High / 720p+</a> ";
	}
	if (isset($blob["SD"])) {
		echo "<a href='{$blob['SD']}' class='label label-info'>Standard / 480p</a> ";
	}
	echo "</td></tr>";
}
echo "</table>"
?>