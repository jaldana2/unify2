<?php
requireLogin();
$urusai = new Urusai($mysqli);

echo "
<a href='/app/UrusaiLatte'>Continue</a>
<script>
	location.href = '/app/UrusaiLatte';
</script>
";

if (isset($_GET["anime_id"])) {
	$anime_id = $_GET["anime_id"];
	// show anime info
	$info = $urusai->getAnimeAttributes($anime_id);
	echo "
		<style type='text/css'>
			body { background-image: linear-gradient(to top, rgba(46, 49, 65, 0.8), rgba(46, 49, 65, 0.8)), url(/urusai/{$anime_id}/fanart.jpg); }
			.wrapper { background-color: rgba(46, 49, 65, 0.9); }
			.wrapper:before, .wrapper:after { opacity: 0.9; }
			@media screen and (max-width: 1280px) {
				#banner, #wrapper > header, #footer { background-image: linear-gradient(to top, rgba(46, 49, 65, 0.8), rgba(46, 49, 65, 0.8)), url(/urusai/{$anime_id}/fanart.jpg); }
			}
		</style>
	";
	if (file_exists("data/urusai/banner/{$anime_id}.jpg")) {
		echo "
			<a href='/app/Urusai?anime_id={$anime_id}'><img src='/urusai/{$anime_id}/banner.jpg' alt=\"{$info['title']}\" style='width: 100%;' /></a>
		";
	}
	echo "
		<h2><a href='/app/Urusai?anime_id={$anime_id}'>{$info['title']} ({$info['air_season']})</a></h2>
		<h4>Episodes: {$info['num_episodes']}</h4>
		<div class='well' style='font-size: 0.9em;'>{$info['synopsis']}</div>
		<p>
	";
	foreach (explode(",", $info["genres"]) as $genre) {
		echo "<span class='label label-info'>{$genre}</span> ";
	}
	echo "
		</p>
	";
	
	if (isset($_GET["episode_id"])) {
		$episode_id = $_GET["episode_id"];
		// list mirrors
		$blob = $urusai->getEpisode($episode_id);
		echo "
			<h3>Season {$blob['season']} - Episode {$blob['episode']} ({$blob['absolute_episode']}: {$blob['episode_title']}</h3>
			<small>{$blob['airdate']}</small>
			<br />
			<p>{$blob['description']}</p>
			<hr />
		";
		$mirrors = $urusai->getEpisodeMirrors($episode_id);
		if (count($mirrors) >= 1) {
			echo "
				<video class='afterglow' id='urusai_media' width='1280' height='720' data-autoresize='fit'>
			";
			foreach ($mirrors as $file_id => $file_blob) {
				//echo "<a class='button' href='{$file_blob['source']}'>{$file_blob['quality']}</a><br />";
				if ($file_blob["quality"] == "HD") {
					echo "<source src='{$file_blob['source']}' type='video/mp4' data-quality='hd'>";
				}
				else {
					echo "<source src='{$file_blob['source']}' type='video/mp4'>";
				}
			}
			echo "</video>";
			echo "Direct Links: ";
			foreach ($mirrors as $file_blob) {
				echo "<a href='{$file_blob['source']}' class='label label-warning' style='padding: 4px 10px;'>{$file_blob['quality']}</a> ";
			}
		}
		else {
			echo "<h4>This episode is currently not available.</h4>";
		}
	}
	else {
		// list episodes
		$episodes = $urusai->getEpisodes($anime_id);
		
		foreach ($episodes as $episode_num => $blob) {
			echo "<a class='btn' href='/app/Urusai?anime_id={$anime_id}&episode_id={$blob['id']}'>{$blob['season']}x{$blob['episode']} ({$blob['absolute_episode']}) &mdash; {$blob['episode_title']} ({$blob['airdate']})</a><br />";
		}
	}
}
else {
	// list series
	$listing = $urusai->getListing();

	echo "
		<h2 class='major'>Recently Added</h2>
		<table class='urusai-calendar'>
	";
	
	$allEps = $urusai->getLatestEpisodes();
	// generate mini calendar
	$recalledDates = array();
	$animeTitles = array();
	$maxDatesPrior = 6;
	foreach ($allEps as $blob) {
		if (!isset($recalledDates[$blob["airdate"]])) {
			if (count($recalledDates) >= $maxDatesPrior) break;
			echo "<tr><td colspan='2'><b>{$blob['airdate']}</b></td></tr>";
			$recalledDates[$blob["airdate"]] = true;
		}
		if (!isset($animeTitles[$blob["anime_id"]])) {
			$animeTitles[$blob["anime_id"]] = $urusai->getAnimeAttribute($blob["anime_id"], "title", SQLITE3_TEXT);
		}
		echo "
			<tr class='urusai-calendar-episode' data-href='/app/Urusai?anime_id={$blob['anime_id']}&episode_id={$blob['id']}'><td style='min-width: 30%; max-width: 40%; text-align: right;'><img src='/urusai/{$blob['anime_id']}/banner.jpg' style='height: 48px;' /><br/><b>{$animeTitles[$blob['anime_id']]}</b></td><td>ep.<span style='font-size: 2em;'>{$blob['episode']}</span> &mdash; {$blob['episode_title']}</td></tr>
		";
	}
	
	echo "
		</table>
	";
	
	//foreach ($listing as $anime_id => $title) {
	//	echo "<a class='btn' href='#anime_{$anime_id}'>{$title}</a><br />";
	//}
	
	echo "
		<section class='wrapper alt style1'>
			<div class='inner'>
				<section class='features'>
	";
	foreach ($listing as $anime_id => $title) {
		$attrs = $urusai->getAnimeAttributes($anime_id);
		if ($attrs["status"] == -1) continue;
		//$synopsis = $urusai->getAnimeAttribute($anime_id, "synopsis", SQLITE3_TEXT);
		echo "
					<article>
						<a href='/app/Urusai?anime_id={$anime_id}' class='image' name='anime_{$anime_id}' style='text-align: center;'><img src='/urusai/{$anime_id}/poster.jpg' alt=\"{$title}\" style='max-width: 100%; max-height: 480px; display: inline-block; width: auto; margin: auto; border-radius: 0;'/></a>
						<h3 class='major'>{$title}</h3>
						<p style='font-size: 0.8em;'>{$attrs['synopsis']}</p>
						<a href='/app/Urusai?anime_id={$anime_id}' class='special'>Browse episodes</a>
					</article>
		";
	}
	echo "
				</section>
			</div>
		</section>
	";
}
?>