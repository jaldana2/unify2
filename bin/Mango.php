<?php
requireLogin();

$do = "default";
if (isset($_GET["do"])) $do = $_GET["do"];

switch ($do) {
	case "view":
		$manga = $_GET["manga"];
		$chapter = $_GET["chapter"];
		$dir = "data/mango/{$manga}/{$chapter}/";
		$length = count(dir_get($dir));
		
		echo "
			<script>
				var dir = \"{$dir}\";
				var length = {$length};
				var current = 1;
				var notificationSent = false;
				var manga = \"{$manga}\";
				var chapter = \"{$chapter}\";
				
				var files = ['--'];
		";
		
		foreach (array_diff(scandir($dir), array(".", "..")) as $file) {
			printf("files.push('%s');", transferGen("data/mango/{$manga}/{$chapter}/{$file}"));
		}
		
		echo "
			</script>
		
			<div id='mangaBox'>
				<div id='mangaMax'>
					<img id='mangaImage' onclick=\"current++; loadPage(current);\" />
				</div>
				<div id='mangaControl' class='text-light-blue'>
					<i class='fa fa-arrow-left' onclick=\"current--; loadPage(current);\"></i>&nbsp;
					<span id='currentNum'>1</span> of {$length}
					<i class='fa fa-arrow-right' onclick=\"current++; loadPage(current);\"></i>
					<br />
					{$chapter}
				</div>
			</div>
		";
	break;
	default:
		// list
		
		echo "
			<div id='mangaListing'>
		";
		
		$listing = dir_get("data/mango");
		natsort($listing);
		foreach ($listing as $node) {
			$title = basename($node);
			$titleSafe = cleanANString($title);
			
			echo "
				<h3><a data-toggle='collapse' data-parent='#mangaListing' href='#manga{$titleSafe}'>{$title}</a></h3>
				<div id='manga{$titleSafe}' class='panel-collapse collapse'>
					<select class='cpicker form-control' base-url='?app=Mango&do=view&manga={$title}&chapter=' onchange=\"location.href = $(this).attr('base-url') + $(this).val();\">
						<option>Select chapter...</option>
			";
			
			$chapters = dir_get($node);
			foreach ($chapters as $chapter) {
				$ctitle = basename($chapter);
				echo "<option value='{$ctitle}'>{$ctitle}</option>";
			}
			
			echo "
					</select>
				</div>
				<hr />
			";
		}
		
		echo "
			</div>
		";
	break;
}
?>