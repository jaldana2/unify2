<!-- One -->
	<section id="one" class="wrapper spotlight style1">
		<div class="inner">
			<a href="/app/UrusaiLatte" class="image"><img src="/images/touhou_chibi.png" alt="" /></a>
			<div class="content">
				<h2 class="major"><span style="text-transform: lowercase; font-weight: normal;">urusai.ninja &mdash; </span>Anime</h2>
				<p>Watch a very select collection of anime, with friends (if that's what you prefer).</p>
				<a href="/app/Urusai" class="special">Open the world of kawaiiness.</a>
			</div>
		</div>
	</section>

<!-- Two -->
	<section id="two" class="wrapper alt spotlight style2">
		<div class="inner">
			<a href="/app/Tokyo" class="image"><img src="/images/ambient_music.jpg" alt="" /></a>
			<div class="content">
				<h2 class="major"><span style="text-transform: lowercase; font-weight: normal;">tokyo &mdash; </span>Music</h2>
				<p>Want to listen to songs but you're not sure what to listen to? Check out some of our favorites!</p>
				<a href="/app/Tokyo" class="special">Ahh... Music to my ears.</a>
			</div>
		</div>
	</section>

<!-- Three -->
	<section id="three" class="wrapper spotlight style3">
		<div class="inner">
			<a href="/app/Gallery" class="image"><img src="/images/abstract_colors.jpg" alt="" /></a>
			<div class="content">
				<h2 class="major">Gallery</h2>
				<p>Some cherry picked wallpapers and images gathered throughout the internets.</p>
				<a href="/app/Gallery" class="special">Browse the collection.</a>
			</div>
		</div>
	</section>

<!-- Four -->
	<section id="four" class="wrapper alt style1">
		<div class="inner">
			<h2 class="major">Even more...</h2>
			<!--
			<p>This site is a creative playground made by JP/kuru. To be honest there is no real purpose besides experimenting with web development and playing around with styles and snippets of code here and there. Here are some more random things that you can do on <span style="text-transform: lowercase; font-weight: normal;">after|<b>mirror</b></span>!</p>
			-->
			<section class="features">
				<article>
					<a href="/app/Mango" class="image"><img src="/images/banner_mango.jpg" alt="" /></a>
					<h3 class="major"><span style="text-transform: lowercase; font-weight: normal;">mango &mdash; </span>Manga Reader</h3>
					<p>A lightweight manga reader for a very select collection of Japanese manga.</p>
					<a href="/app/Mango" class="special">Read all about it.</a>
				</article>
				<article>
					<a href="/app/Storage" class="image"><img src="/images/banner_cloud.jpg" alt="" /></a>
					<h3 class="major">Cloud</h3>
					<p>A small cloud file storage for personal use.</p>
					<a href="/app/Storage" class="special">Explore rainbows.</a>
				</article>
			</section>
			<!--
			<ul class="actions">
				<li><a href="/app/All" class="button">Browse All</a></li>
			</ul>
			-->
		</div>
	</section>