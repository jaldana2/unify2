<?php
requireLogin();
requireAccess($auth, AUTH_ACCESS_ADMIN);

$urusai = new Urusai($mysqli);

$mirror_id = isset($_GET["mirror_id"]) ? $_GET["mirror_id"] : -1;
$episode_id = isset($_GET["episode_id"]) ? $_GET["episode_id"] : -1;
$anime_id = isset($_GET["anime_id"]) ? $_GET["anime_id"] : -1;
echo "<a href='/app/UrusaiManager' class='button'>Back to Home</a> ";
if ($anime_id >= 0) {
	echo "<a href='/app/UrusaiManager?do=episodes&anime_id={$anime_id}' class='button'>Back to Episodes</a> ";
}
if ($episode_id >= 0) {
	echo "<a href='/app/UrusaiManager?do=mirrors&anime_id={$anime_id}&episode_id={$episode_id}' class='button'>Back to Mirrors</a> ";
}

if (isset($_GET["do"])) switch($_GET["do"]) {
	case "new":
		if (isset($_POST["title"])) {
			$urusai->addAnime($_POST["title"], $_POST["alt_title"], $_POST["air_season"], $_POST["num_episodes"], $_POST["genres"], $_POST["ranking"], $_POST["score"], $_POST["synopsis"], $_POST["airing"], $_POST["status"]);
			$anime_id = $urusai->getAnimeID($_POST["title"]);
			if ($_FILES["banner"]["size"] > 0 && $_FILES["banner"]["error"] == 0) {
				iTF($_FILES["banner"]["tmp_name"], "data/urusai/banner/{$anime_id}.jpg", 2000, 94);
			}
			if ($_FILES["poster"]["size"] > 0 && $_FILES["poster"]["error"] == 0) {
				iTF($_FILES["poster"]["tmp_name"], "data/urusai/poster/{$anime_id}.jpg", 1000, 92);
			}
			if ($_FILES["fanart"]["size"] > 0 && $_FILES["fanart"]["error"] == 0) {
				iTF($_FILES["fanart"]["tmp_name"], "data/urusai/fanart/{$anime_id}.jpg", 2000, 94);
			}
			echo "<h2>{$_POST['title']} added!</h2>";
		}
		echo "
			<h2>New Anime</h2>
			<form action='/app/UrusaiManager&do=new' method='post' enctype='multipart/form-data'>
				<table>
					<tr><td>Title</td><td><input type='text' name='title' value='' /></td></tr>
					<tr><td>Alternate Title</td><td><input type='text' name='alt_title' value='' /></td></tr>
					<tr><td>Airing Season</td><td><input type='text' name='air_season' value='' /></td></tr>
					<tr><td>Number of Episodes</td><td><input type='text' name='num_episodes' value='' /></td></tr>
					<tr><td>Genres</td><td><input type='text' name='genres' value='' /></td></tr>
					<tr><td>Ranking</td><td><input type='text' name='ranking' value='' /></td></tr>
					<tr><td>Score</td><td><input type='text' name='score' value='' /></td></tr>
					<tr><td>Synopsis</td><td><input type='text' name='synopsis' value='' /></td></tr>
					<tr><td>Airing</td><td><input type='text' name='airing' value='' /></td></tr>
					<tr><td>Status</td><td><input type='text' name='status' value='' /></td></tr>
					<tr><td>Banner</td><td><input type='file' name='banner' /></td></tr>
					<tr><td>Poster</td><td><input type='file' name='poster' /></td></tr>
					<tr><td>Fanart</td><td><input type='file' name='fanart' /></td></tr>
				</table>
				<input type='submit' value='Add' />
			</form>
		";
	break;
	case "edit":
		if (isset($_POST["title"])) {
			$urusai->updateAnime($anime_id, $_POST["title"], $_POST["alt_title"], $_POST["air_season"], $_POST["num_episodes"], $_POST["genres"], $_POST["ranking"], $_POST["score"], $_POST["synopsis"], $_POST["airing"], $_POST["status"]);
			if ($_FILES["banner"]["size"] > 0 && $_FILES["banner"]["error"] == 0) {
				iTF($_FILES["banner"]["tmp_name"], "data/urusai/banner/{$anime_id}.jpg", 2000, 94);
			}
			if ($_FILES["poster"]["size"] > 0 && $_FILES["poster"]["error"] == 0) {
				iTF($_FILES["poster"]["tmp_name"], "data/urusai/poster/{$anime_id}.jpg", 1000, 92);
			}
			if ($_FILES["fanart"]["size"] > 0 && $_FILES["fanart"]["error"] == 0) {
				iTF($_FILES["fanart"]["tmp_name"], "data/urusai/fanart/{$anime_id}.jpg", 2000, 94);
			}
			echo "<h2>{$_POST['title']} updated!</h2>";
		}
		
		$info = $urusai->getAnimeAttributes($anime_id);
		echo "
			<h2>ID: {$anime_id} &mdash; {$info['title']}</h2>
			<a href='/app/UrusaiManager?do=delete&anime_id={$anime_id}' style='color: red;'>Delete anime</a>
			<form action='/app/UrusaiManager&do=edit&anime_id={$anime_id}' method='post' enctype='multipart/form-data'>
				<table>
					<tr><td>Title</td><td><input type='text' name='title' value='{$info['title']}' /></td></tr>
					<tr><td>Alternate Title</td><td><input type='text' name='alt_title' value='{$info['alt_title']}' /></td></tr>
					<tr><td>Airing Season</td><td><input type='text' name='air_season' value='{$info['air_season']}' /></td></tr>
					<tr><td>Number of Episodes</td><td><input type='text' name='num_episodes' value='{$info['num_episodes']}' /></td></tr>
					<tr><td>Genres</td><td><input type='text' name='genres' value='{$info['genres']}' /></td></tr>
					<tr><td>Ranking</td><td><input type='text' name='ranking' value='{$info['ranking']}' /></td></tr>
					<tr><td>Score</td><td><input type='text' name='score' value='{$info['score']}' /></td></tr>
					<tr><td>Synopsis</td><td><input type='text' name='synopsis' value='{$info['synopsis']}' /></td></tr>
					<tr><td>Airing</td><td><input type='text' name='airing' value='{$info['airing']}' /></td></tr>
					<tr><td>Status</td><td><input type='text' name='status' value='{$info['status']}' /></td></tr>
					<tr><td>Banner</td><td><img src='/urusai/{$anime_id}/banner.jpg' style='max-width: 100%;' /><br /><input type='file' name='banner' /></td></tr>
					<tr><td>Poster</td><td><img src='/urusai/{$anime_id}/poster.jpg' style='max-width: 100%;' /><br /><input type='file' name='poster' /></td></tr>
					<tr><td>Fanart</td><td><img src='/urusai/{$anime_id}/fanart.jpg' style='max-width: 100%;' /><br /><input type='file' name='fanart' /></td></tr>
				</table>
				<input type='submit' value='Save' />
			</form>
		";
	break;
	case "episodes":
		$episodes = $urusai->getEpisodes($anime_id);
		echo "
			<a href='/app/UrusaiManager?do=new_episode&anime_id={$anime_id}' class='button'>New Episode</a>
			<br />
			<table>
				<tr><th>ID</th><th>Number</th><th>Title</th><th>Edit</th><th>Mirrors</th></tr>
		";
		foreach ($episodes as $blob) {
			echo "
				<tr><td>{$blob['id']}</td><td>{$blob['episode']}</td><td>{$blob['episode_title']}</td><td><a href='/app/UrusaiManager?do=edit_episode&episode_id={$blob['id']}&anime_id={$anime_id}'>Edit</a></td><td><a href='/app/UrusaiManager?do=mirrors&episode_id={$blob['id']}&anime_id={$anime_id}'>Mirrors</a></td></tr>
			";
		}
		echo "
			</table>
		";
	break;
	case "edit_episode":
		if (isset($_POST["episode"])) {
			$urusai->updateEpisode($episode_id, $anime_id, $_POST["episode"], $_POST["episode_title"], $_POST["description"], $_POST["airdate"]);
			echo "<h2>{$_POST['episode_title']} updated!</h2>";
		}
		
		$episode = $urusai->getEpisode($episode_id);
		echo "
			<h2>ID: {$episode_id} &mdash; {$episode['episode_title']}</h2>
			<a href='/app/UrusaiManager?do=delete_episode&anime_id={$anime_id}&episode_id={$episode_id}' style='color: red;'>Delete episode</a>
			<form action='/app/UrusaiManager&do=edit_episode&episode_id={$episode_id}&anime_id={$anime_id}' method='post'>
				<table>
					<tr><td>Episode Number</td><td><input type='text' name='episode' value='{$episode['episode']}' /></td></tr>
					<tr><td>Episode Title</td><td><input type='text' name='episode_title' value='{$episode['episode_title']}' /></td></tr>
					<tr><td>Description</td><td><input type='text' name='description' value='{$episode['description']}' /></td></tr>
					<tr><td>Airdate (YYYY-MM-DD)</td><td><input type='text' name='airdate' value='{$episode['airdate']}' /></td></tr>
				</table>
				<input type='submit' value='Save' />
			</form>
		";
	break;
	case "new_episode":
		if (isset($_POST["episode"])) {
			$urusai->addEpisode($anime_id, $_POST["episode"], $_POST["episode_title"], $_POST["description"], $_POST["airdate"]);
			echo "<h2>{$_POST['episode_title']} added!</h2>";
		}
		
		echo "
			<h2>New Episode</h2>
			<form action='/app/UrusaiManager&do=new_episode&anime_id={$anime_id}' method='post'>
				<table>
					<tr><td>Episode Number</td><td><input type='text' name='episode' value='' /></td></tr>
					<tr><td>Episode Title</td><td><input type='text' name='episode_title' value='' /></td></tr>
					<tr><td>Description</td><td><input type='text' name='description' value='' /></td></tr>
					<tr><td>Airdate (YYYY-MM-DD)</td><td><input type='text' name='airdate' value='' /></td></tr>
				</table>
				<input type='submit' value='Save' />
			</form>
		";
	break;
	case "mirrors":
		$mirrors = $urusai->getEpisodeMirrors($episode_id);
		echo "
			<a href='/app/UrusaiManager?do=new_mirror&anime_id={$anime_id}&episode_id={$episode_id}' class='button'>New Mirror</a>
			<br />
			<table>
				<tr><th>ID</th><th>Source</th><th>Quality</th><th>Edit</th></tr>
		";
		foreach ($mirrors as $id => $blob) {
			echo "
				<tr><td>{$id}</td><td>{$blob['source']}</td><td>{$blob['quality']}</td><td><a href='/app/UrusaiManager?do=edit_mirror&mirror_id={$id}&episode_id={$episode_id}&anime_id={$anime_id}'>Edit</a></td></tr>
			";
		}
		echo "
			</table>
		";
	break;
	case "edit_mirror":
		
		if (isset($_POST["source"])) {
			$urusai->updateEpisodeMirror($mirror_id, $episode_id, $_POST["source"], $_POST["quality"]);
			echo "<h2>Mirror updated!</h2>";
		}
		
		$mirror = $urusai->getEpisodeMirror($mirror_id);
		echo "
			<h2>ID: {$mirror_id}</h2>
			<a href='/app/UrusaiManager?do=delete_mirror&mirror_id={$mirror_id}&anime_id={$anime_id}&episode_id={$episode_id}' style='color: red;'>Delete mirror</a>
			<form action='/app/UrusaiManager&do=edit_mirror&mirror_id={$mirror_id}&episode_id={$episode_id}&anime_id={$anime_id}' method='post'>
				<table>
					<tr><td>Source</td><td><input type='text' name='source' value='{$mirror['source']}' /></td></tr>
					<tr><td>Quality</td><td><input type='text' name='quality' value='{$mirror['quality']}' /></td></tr>
				</table>
				<input type='submit' value='Save' />
			</form>
		";
	break;
	case "new_mirror":
		if (isset($_POST["source"])) {
			$urusai->addEpisodeMirror($episode_id, $_POST["source"], $_POST["quality"]);
			echo "<h2>Mirror added!</h2>";
		}
		
		echo "
			<h2>New Mirror</h2>
			<form action='/app/UrusaiManager&do=new_mirror&episode_id={$episode_id}&anime_id={$anime_id}' method='post'>
				<table>
					<tr><td>Source</td><td><input type='text' name='source' value='' /></td></tr>
					<tr><td>Quality</td><td><input type='text' name='quality' value='' /></td></tr>
				</table>
				<input type='submit' value='Save' />
			</form>
		";
	break;
	case "delete":
		$urusai->deleteAnime($anime_id);
		echo "<h3>Anime deleted.</h3>";
	break;
	case "delete_episode":
		$urusai->deleteEpisode($episode_id);
		echo "<h3>Episode deleted.</h3>";
	break;
	case "delete_mirror":
		$urusai->deleteMirror($mirror_id);
		echo "<h3>Mirror deleted.</h3>";
	break;
}
else {
	$listing = $urusai->getListing();
	echo "
		<a href='/app/UrusaiManager?do=new' class='button'>New Anime</a>
		<br />
		<table>
			<tr><th>ID</th><th>Title</th><th>Edit</th><th>Episodes</th><th>Status</th></tr>
	";
	foreach ($listing as $id => $title) {
		echo "
			<tr><td>{$id}</td><td>{$title}</td><td><a href='/app/UrusaiManager?do=edit&anime_id={$id}'>Edit</a></td><td><a href='/app/UrusaiManager?do=episodes&anime_id={$id}'>Episodes</a></td><td><img src='/urusai/{$id}/poster.jpg' style='max-height: 32px;' /><br /><img src='/urusai/{$id}/banner.jpg' style='max-height: 32px;' /><br /><img src='/urusai/{$id}/fanart.jpg' style='max-height: 32px;' /></td></tr>
		";
	}
	echo "
		</table>
	";
}
?>