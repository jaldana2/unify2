<?php
requireLogin();

echo "
	<h1 class='app-title'><a href='/app/Store' class='content-link' data-title='Store'><i class='fa fa-shopping-cart'></i> Store</a></h1>
	<p class='subtitle'>Ooh, shinies.</p>
";

$points = $auth->getPoints(AUTH_USER);

echo "
	<p><b>Your Points</b>: {$points}</p>
";
?>