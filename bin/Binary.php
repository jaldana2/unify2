<style>
	code { color: pink; }
	small { font-size: 0.8em; }
</style>
<h1>How to convert binary to 'letters'</h1>
<div class="row">
	<div class="col-sm-6">
		<h3>0) Binary</h3>
		<p>Let's say you wanted to convert this mess of numbers...</p>
	</div>
	<div class="col-sm-6">
		<h3>(example)</h3>
		<pre><code>01001000011001010110110001101100011011110010110000100000010000010111010001110100011000010110111000100001</code></pre>
	</div>
	<div class="col-sm-6">
		<h3>1) (pre) Binary to Decimal</h3>
		<p>Each <em>character</em> is 1 <em>byte</em> or 8 <em>bits</em> long.</p>
		<p>For example, <u>hello</u> would be 5 bytes or 5x8 = 40 bits long.</p>
		<p>So in order to 'translate' the example above, we'll need to split it into 1 byte or 8 bit chunks, like so:</p>
	</div>
	<div class="col-sm-6">
		<h3>(example)</h3>
		<pre><code>01001000
01100101
01101100
01101100
01101111
00101100
00100000
01000001
01110100
01110100
01100001
01101110
00100001</code></pre>
	</div>
	<div class="col-sm-6">
		<h3>1.1) Binary to Decimal</h3>
		<p>Okay, now that it's a bit more cleaner, we need to convert each of them to a number.</p>
		<p>Binary is simply a number in base-2 (compared to the standard numbers which are in base-10) so conversion from binary to decimal can easily be done with a bit of math (or memorization)</p>
		<table>
			<tr><th>Binary (base-2)</th><th>Decimal (base-10)</th></tr>
			<tr><td><code>00000000</code><td><code>0</code></td></tr>
			<tr><td><code>00000001</code><td><code>1</code></td></tr>
			<tr><td><code>00000010</code><td><code>2</code></td></tr>
			<tr><td><code>00000011</code><td><code>3</code></td></tr>
			<tr><td><code>00000100</code><td><code>4</code></td></tr>
		</table>
		<p>Now instead of listing an entire table with 0 to 255, here's the mathematical way of doing it.</p>
		<hr />
		<p>Each digit corresponds to a different power of 2.</p>
		<p><code>_ _ _ _ _ _ _ X</code> is 2<sup>0</sup>.</p>
		<p><code>_ _ _ _ _ _ X _</code> is 2<sup>1</sup>.</p>
		<p><code>_ _ _ _ _ X _ _</code> is 2<sup>2</sup>.</p>
		<p>Using decimal 5 (00000101) as an example, we see that the 'one's and the 'hundred's column have a 1 in it. Using the powers of two, <pre><code>00000101
	= 2<sup>0</sup> + 2<sup>2</sup>
	= 1 + 4
	= 5</code></pre></p>
		<p>With bigger binary values such as <code>01100101</code>, the same pattern exists.<pre><code>01100101
	= 2<sup>6</sup> + 2<sup>5</sup> + 2<sup>2</sup> + 2<sup>0</sup>
	= 64 + 32 + 4 + 1
	= 101</code></pre></p>
		<p>Not too bad, huh? <i class="fa fa-smile-o"></i></p>
	</div>
	<div class="col-sm-6">
		<h3>(example)</h3>
		<pre><code>01001000 = 72
01100101 = 101
01101100 = 108
01101100 = 108
01101111 = 111
00101100 = 44
00100000 = 32
01000001 = 65
01110100 = 116
01110100 = 116
01100001 = 97
01101110 = 110
00100001 = 33</code></pre>
	</div>
	<div class="col-sm-12"></div>
	<div class="col-sm-6">
		<h3>2) What do all these numbers mean?!</h3>
		<p>So with our running example, we're left with:</p>
		<pre><code>72, 101, 108, 108, 111, 44, 32, 65, 116, 116, 97, 110, 33</code></pre>
		<p>Now we must use something called an ASCII table to convert these decimal numbers into normal characters.</p>
		<img src="/images/asciifull.gif" data-featherlight="/images/asciifull.gif" style="width: 100%;">
		<small>Click the image above to view a larger version.</small>
		<p>Decimal 0 - 31 aren't normal human-readable characters, so ignoring those, the first character in the ASCII table is decimal 32 or binary <code>00100000</code> which is the character for: <code> </code> (space).</p>
		<p>The best way to go about this is to remember that ASCII 65 is capital A, and ASCII 97 is lowercase a.</p>
		<p>Then, if you have a decimal value of say, 108. You can use the reference point of ASCII 97 = a to figure it out.</p>
		<pre><code>108
	= 97 + 11
	= 'a' + 11 (11 letters after 'a')
	= 'a' + ...b...c...d...e...f...g...h...i...j...k...l!
	= 'l'</code></pre>
	</div>
	<div class="col-sm-6"></div>
	<div class="col-sm-12"></div>
	<div class="col-sm-6">
		<p>Yep, converting from ASCII to binary is just this whole process but reversed.</p>
		<p><i class="fa fa-warning"></i> Bored? Try to finish the conversion for the example on the right.</p>
	</div>
	<div class="col-sm-6">
		<h3>(example)</h3>
		<pre><code>01001000 = 72  = ?
01100101 = 101 = ?
01101100 = 108 = l
01101100 = 108 = l
01101111 = 111 = ?
00101100 = 44  = ?
00100000 = 32  = ?
01000001 = 65  = ?
01110100 = 116 = ?
01110100 = 116 = ?
01100001 = 97  = ?
01101110 = 110 = ?
00100001 = 33  = ?</code></pre>
	</div>
</div>