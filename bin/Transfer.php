<?php
$v = @fnDecrypt(base64_decode($_GET["v"]));
if ($v) {
	$timestamp = substr($v, strripos($v, "|") + 1);
	$file = substr($v, 0, strripos($v, "|"));
	$timedelta = time() - $timestamp;
	if ($timedelta <= 3600) {
		if (file_exists($file)) {
			if (isset($_GET["download"])) {
				forceDL($file);
			}
			else {
				if (isset($_GET["hx"])) {
					header("Content-Type: {$_GET['hx']}");
				}
				else {
					$fext = fext($file);
					if (fextIsImage($fext)) header("Content-Type: image/jpg");
					if (fextIsVideo($fext)) header("Content-Type: video/mp4");
					if (fextIsMusic($fext)) header("Content-Type: audio/mp3");
				}
				readfile($file);
			}
		}
		else {
			header("Location: {$file}");
		}
	}
	else {
		die("Expired link.");
	}
}
else {
	die("Invalid request.");
}
?>