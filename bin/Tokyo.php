<?php
requireLogin();
$tokyo = new Tokyo($mysqli);

echo "
	<div class='tokyo-tabs'>
		<label id='btn-listing' class='btn-tab' data-tab='tokyo-listing-ctr'>Music</label>
		<label id='btn-artists' class='btn-tab' data-tab='tokyo-artists'>Artists</label>
		<label id='btn-tags' class='btn-tab' data-tab='tokyo-tags'>Tags</label>
		<label id='btn-playlists' class='btn-tab' data-tab='tokyo-playlists'>Playlists</label>
		<label id='btn-upload' class='btn-tab' data-tab='tokyo-upload'>Upload</label>
	</div>
	<div class='tokyo-main'>
		<div id='tokyo-listing-ctr'>
			<table id='tokyo-listing'>
				<thead>
					<tr><th style='width: 48px;'></th><th>Name</th><th>Time</th><th>Artist</th><th>Tags</th></tr>
				</thead>
				<tbody>
";
$listing = $tokyo->getListing();
foreach ($listing as $blob) {
	$duration = sec2mmss($blob["duration"]);
	$tags = "";
	$tags_csv = "";
	foreach ($tokyo->getMusicTags($blob["mediaID"]) as $tag) {
		$tags .= "<span class='label label-info' style='color: black;'>{$tag}</span> ";
		$tags_csv .= "{$tag},";
	}
	$tags_csv = substr($tags_csv, 0, -1);
	echo "
		<tr class='tokyo-media' data-media-id='{$blob['mediaID']}' data-title=\"{$blob['title']}\" data-artist=\"{$blob['artist']}\" data-tags=\"{$tags_csv}\" data-thumb=\"{$blob['artwork']}\"><td class='tokyo-thumb-container'><div class='tokyo-thumb b-lazy optional' data-src='{$blob['artwork']}'></div></td><td><i class='fa fa-play tokyo-play-icon'></i>&nbsp; {$blob['title']}</td><td>{$duration}</td><td>{$blob['artist']}</td><td>{$tags}</td></tr>
	";
}
echo "
				</tbody>
			</table>
		</div>
		<div id='tokyo-artists' style='display: none; padding: 8px;'>
";
$artists = $tokyo->getMusicArtists();
foreach ($artists as $artist) {
	echo "
		<input type='checkbox' class='tokyo-artist-toggle' data-artist=\"{$artist}\" data-labelauty=\"{$artist}\" checked /> 
	";
}
echo "
			<p>Showing <span class='tokyo-active-count'></span> of <span class='tokyo-total-count'></span>.</p>
		</div>
		<div id='tokyo-tags' style='display: none; padding: 8px;'>
";
$tags = $tokyo->getTagListing();
foreach ($tags as $tag) {
	echo "
		<input type='checkbox' class='tokyo-tag-toggle' data-tag=\"{$tag}\" data-labelauty=\"{$tag}\" checked /> 
	";
}
echo "
			<p>Showing <span class='tokyo-active-count'></span> of <span class='tokyo-total-count'></span>.</p>
		</div>
		<div id='tokyo-playlists' style='display: none;'>
			<p>Playlists are currently not available.</p>
		</div>
		<div id='tokyo-upload' style='display: none; padding: 8px;'>
			<h3>Upload Music</h3>
			<form action='/app/Tokyo' method='post' id='upload-frm'>
				<input type='text' id='upload-url' placeholder='YouTube URL' />
				<input type='hidden' name='ytid' id='upload-id' value='' />
				<div id='upload-ctl' style='text-align: center; display: none;'>
					<img src='' id='upload-thumb' />
					<input type='text' name='title' id='upload-title' placeholder='Title' maxlength='128' />
					<input type='text' name='artist' id='upload-artist' placeholder='Artist' maxlength='128' />
					<select class='select2-compat' name='tags[]' id='upload-tags' multiple style='width: 100%;'>
";
//$tags = $tokyo->getTagListing(); // defined a few lines above...
foreach ($tags as $tag) {
	echo "<option value='{$tag}'>{$tag}</option>";
}
echo "
					</select>
					<input type='submit' id='upload-btn' value='Upload' />
				</div>
			</form>
		</div>
	</div>
	<div id='tokyo-control'>
		<div id='aAlbumArt' class='optional' style='display: none;'></div>
		<div id='aPlayText-container' class='optional' style='display: none;'><span id='aPlayText'></span></div>
		<span class='fa fa-play' id='mPlayPause'></span>
		<input type='range' id='mSeeker' min='0' max='1' style='min-width: 140px; width: 35%;' class='tokyo' />
		<span id='tPosition'>0:00</span>
		<span class='fa fa-forward' id='hForward'></span>
		&nbsp;&nbsp;
		<span class='fa fa-volume-down optional' id='hVolDown'></span>
		<input type='range' id='mVol' min='0' max='1' step='0.01' value='1' style='width: 64px;' class='tokyo' />
		<span class='fa fa-volume-up' id='hVolUp'></span>
		&nbsp;&nbsp;
		<span class='fa fa-repeat optional' id='sRepeat'></span>
		<span class='fa fa-random optional' id='sShuffle'></span>
	</div>
	<audio id='mediaPlayer' style='display: none;'></audio>
	<input type='checkbox' id='optRepeat' data-labelauty='Repeat' />
	<input type='checkbox' id='optShuffle' data-labelauty='Shuffle' />
";
?>