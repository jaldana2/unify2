<?php
requireLogin();

echo "
	<h3 class='major'>Site Settings</h3>
	<p>These settings will change how the site responds.</p>
	<br />
	<input type='checkbox' id='settingsUsePageTransition' data-labelauty='Enable Page Transitions' />
";
?>