<?php
requireLogin();

$my_notifications = $notify->getNotifications(AUTH_UID);
if (isset($_GET["delete"]) && isset($my_notifications[$_GET["delete"]])) {
	// delete my own notification
	$notify->deleteNotification($_GET["delete"]);
	//$my_notifications = pushValueFromArray($_GET["delete"], $my_notifications, true);
	unset($my_notifications[$_GET["delete"]]);
}
if (is_array($my_notifications) && count($my_notifications) >= 1) foreach ($my_notifications as $id => $blob) {
	echo "
		<div class='well'>
			<div style='float: right;'>
				<a href='/app/Notify?delete={$id}'><i class='fa fa-close' style='color: red;'></i></a>
			</div>
			<h3><a href='{$blob['action']}'>{$blob['title']}</a></h3>
			<p>{$blob['content']}</p>
		</div>
	";
}
else {
	echo "<h2>No notifications!</h2>";
}
?>