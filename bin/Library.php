<?php
requireLogin();
$library = new Library($mysqli);

if (isset($_GET["update"]) && isset($_POST["bid"]) && isset($_POST["pos"])) {
	$library->savePosition(AUTH_UID, (int) $_POST["bid"], (int) $_POST["pos"]);
}
elseif (isset($_GET["book"])) {
	$blob = $library->getVolume($_GET["book"]);
	$book = $blob["source"];
	if ($book == "") {
		echo "<h2>{$blob['title']} is not available, sorry.</h2>";
	}
	else {
		$pos = $library->getPosition(AUTH_UID, $_GET["book"]);
		if ($pos) {
			echo "
				<script>
					var savePage = {$pos['page']};
				</script>
			";
		}
		echo "
			<h3>{$blob['title']}</h3>
			<script src='/assets/js/epub.js'></script>
			<script src='/assets/js/zip.min.js'></script>
			<div class='libraryContainer'>
				<div id='bookArea'></div>
				
				<button onclick='book.prevPage();' style='width: 49%;'><i class='fa fa-arrow-left'></i> Previous</button> 
				<button onclick='book.nextPage();' style='width: 49%;'>Next <i class='fa fa-arrow-right'></i></button>
				Page: <span id='current_page'></span> / <span id='total_page'></span>
				<input class='tokyo' type='range' id='slider' step='1' value='0' />
			</div>
			<script>
				var book = ePub('{$book}', {
					width: 400,
					height: 600,
					styles: {
						fontFamily: \"'Gentium Book Basic', serif\",
						color: '#fefefe',
					},
					restore: true,
					spreads: false
				});
				var rendered = book.renderTo('bookArea');
		";
		if (file_exists("data/library/_json/{$_GET['book']}.txt")) {
			echo "
				console.log('using existing json');
				EPUBJS.core.request('/data/library/_json/{$_GET['book']}.txt').then(function(pageList) {
					book.loadPagination(pageList);
				});
			";
		}
		else {
			echo "
				book.ready.all.then(function() {
					book.generatePagination();
				});
			";
		}
		echo "
				book.pageListReady.then(function(pageList) {
					rendered.then(function() {
						var currentLocation = book.getCurrentLocationCfi();
						var currentPage = book.pagination.pageFromCfi(currentLocation);
						$('#current_page').text(currentPage);
						$('#slider').val(currentPage);
						if (typeof savePage !== 'undefined') {
							book.gotoPage(savePage);
						}
					});
					$('#total_page').text(book.pagination.totalPages);
					$('#slider')
						.attr('min', book.pagination.firstPage)
						.attr('max', book.pagination.lastPage)
						.on('change', function() {
							book.gotoPage($(this).val());
							$('#current_page').text($(this).val());
						});
					if (typeof adminToolsEnabled !== 'undefined' && adminToolsEnabled == true) {
						$('#downloadPageData').show();
						var dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(pageList));
						var dlAnchorElem = document.getElementById('downloadPageData');
						dlAnchorElem.setAttribute('href', dataStr);
						dlAnchorElem.setAttribute('download', \"{$_GET['book']}.txt\");
						//dlAnchorElem.click();
					}
				});
				book.on('book:pageChanged', function(location) {
					$('#current_page').text(location.anchorPage);
					$('#slider').val(location.anchorPage);
					$.post('/app/Library?noUI&update', { bid: {$_GET['book']}, pos: location.anchorPage });
				});
			</script>
		";
		if ($auth->getUserAccess(AUTH_USER) == AUTH_ACCESS_ADMIN) {
			if (!file_exists("data/library/{$_GET['book']}.txt")) {
				echo "
					<a id='downloadPageData' style='display: none;' class='btn'>Download JSON</a>
					<script>
						var adminToolsEnabled = true;
					</script>
				";
			}
		}
	}
}
elseif (isset($_GET["series"])) {
	$series_info = $library->getSeries($_GET["series"]);
	echo "
		<h2>{$series_info['title']}</h2>
		<span>&mdash; {$series_info['author']}</span>
		<hr />
	";
	
	$volumes = $library->getAllVolumes($_GET["series"]);
	echo "
		<div class='row'>
	";
	foreach ($volumes as $blob) {
		echo "
			<div class='col-sm-4' style='text-align: center;'>
		";
		if (file_exists("data/library/_covers/b_{$blob['id']}.jpg")) {
			echo "<a href='/app/Library?book={$blob['id']}'><img src='/data/library/_covers/b_{$blob['id']}.jpg' style='width: 100%;' /><br/><b>#{$blob['volume']}</b><br/>{$blob['title']}</a>";
		}
		else {
			echo "<a href='/app/Library?book={$blob['id']}'><b>#{$blob['volume']}</b><br/>{$blob['title']}</a>";
		}
		echo "
			</div>
		";
	}
	echo "
		</div>
	";
}
else {
	$series = $library->getAllSeries();
	echo "
		<div class='row'>
	";
	foreach ($series as $blob) {
		echo "
			<div class='col-sm-4' style='text-align: center;'>
		";
		if (file_exists("data/library/_covers/s_{$blob['id']}.jpg")) {
			echo "<a href='/app/Library?series={$blob['id']}'><img src='/data/library/_covers/s_{$blob['id']}.jpg' style='width: 100%;' /><br/><b>{$blob['title']}</b><br/>{$blob['author']}</a>";
		}
		else {
			echo "<a href='/app/Library?series={$blob['id']}' class='btn'><b>{$blob['title']}</b><br/>{$blob['author']}</a>";
		}
		echo "
			</div>
		";
	}
	echo "
		</div>
	";
}
?>