<a class="hyperfix" name="login">&nbsp;</a>
<div class="dialog">
	<h1 class="am"><i class="fa fa-lock"></i> login</small></h1>
	<form action="/do/LoginHandler?dm=<?php echo isset($_GET["dm"]) ? $_GET["dm"] : base64_encode($_SERVER["HTTP_HOST"]); ?>&return=<?php if (isset($_GET["return"])) echo $_GET["return"]; ?>" method="post">
		<?php
			if (isset($_GET["error"])) {
				echo "
					<div>Invalid login.</div>
				";
			}
		?>
		<div class="form-group">
			<input type="text" name="loginUser" id="loginUser" placeholder="Login" value="<?php if (isset($_GET["username"])) echo $_GET["username"]; ?>" />
		</div>
		<div class="form-group">
			<input type="password" name="loginPass" id="loginPass" placeholder="Password" />
		</div>
		<div class="form-group">
			<input type="submit" class="button special fit" value="Login" />
		</div>
		<a href="/app/Register?do=register" class="button fit">No account? Register here!</a>
	</form>
</div>