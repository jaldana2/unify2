<?php
requireLogin();
requireAccess($auth, AUTH_ACCESS_ADMIN);
$library = new Library($mysqli);

$series_id = isset($_GET["series_id"]) ? $_GET["series_id"] : -1;
$volume_id = isset($_GET["book_id"]) ? $_GET["book_id"] : -1;
echo "
	<a href='/app/LibraryManager?do=add_series' class='button'>Add Series</a> 
";
if ($series_id >= 0) {
	echo "<a href='/app/LibraryManager?do=add_volumes&series_id={$series_id}' class='button'>Back to Series</a> ";
}
echo "<br/><br/>";

if (isset($_GET["do"])) switch($_GET["do"]) {
	case "add_series":
		if (isset($_POST["title"])) {
			$library->addSeries($_POST["title"], $_POST["author"]);
			echo "<h3>{$_POST['title']} added successfully.</h3>";
		}
		echo "
			<form action='/app/LibraryManager?do=add_series' method='post'>
				<input type='text' name='title' placeholder='Series Title' />
				<input type='text' name='author' placeholder='Author' />
				<input type='submit' value='Add Series' />
			</form>
		";
	break;
	case "add_volumes":
		if (isset($_POST["title"])) {
			if ($_FILES["epub_file"]["size"] > 0 && $_FILES["epub_file"]["error"] == 0) {
				$hash = substr(sha1(uniqid()), 0, 5);
				$file = "/data/library/{$series_id}_{$_POST['volume']}_{$hash}.epub";
				$library->addVolume($series_id, $file, $_POST["title"], $_POST["volume"]);
				move_uploaded_file($_FILES["epub_file"]["tmp_name"], substr($file, 1));
			}
			else {
				$library->addVolume($series_id, $_POST["source"], $_POST["title"], $_POST["volume"]) ;
			}
			echo "<h3>{$_POST['title']} added successfully.</h3>";
		}
		elseif (isset($_POST["s_title"])) {
			$library->updateSeries($series_id, $_POST["s_title"], $_POST["s_author"]);
			if ($_FILES["s_cover"]["size"] > 0 && $_FILES["s_cover"]["error"] == 0) {
				if (!is_dir("data/library/_covers")) mkdir("data/library/_covers");
				iTF($_FILES["s_cover"]["tmp_name"], "data/library/_covers/s_{$series_id}.jpg", 1000, 94);
			}
		}
		
		$series_blob = $library->getSeries($series_id);
		echo "
			<form action='/app/LibraryManager?do=add_volumes&series_id={$series_id}' method='post' enctype='multipart/form-data'>
				<input type='text' name='s_title' value=\"{$series_blob['title']}\" />
				<input type='text' name='s_author' value=\"{$series_blob['author']}\" />
				Series Cover: <img src='/data/library/_covers/s_{$series_id}.jpg' style='height: 320px;' /><br/><input type='file' name='s_cover' />
				<input type='submit' value='Update Series' />
			</form>
		";
		
		echo "
			<form action='/app/LibraryManager?do=add_volumes&series_id={$series_id}' method='post' enctype='multipart/form-data'>
				<input type='text' name='source' placeholder='ePub URL' />
				<input type='text' name='title' placeholder='Title' />
				<input type='text' name='volume' placeholder='Volume #' />
				ePub File: <input type='file' name='epub_file' />
				<input type='submit' value='Add Volume' />
			</form>
		";
		
		foreach ($library->getAllVolumes($series_id) as $blob) {
			$bn = basename($blob["source"]);
			echo "<a href='/app/LibraryManager?do=edit_volume&series_id={$series_id}&book_id={$blob['id']}'>#{$blob['volume']} - {$blob['title']} - {$bn}</a><br />";
		}
	break;
	case "edit_volume":
		if (isset($_POST["title"])) {
			if ($_FILES["epub_file"]["size"] > 0 && $_FILES["epub_file"]["error"] == 0) {
				$hash = substr(sha1(uniqid()), 0, 5);
				$file = "/data/library/{$series_id}_{$_POST['volume']}_{$hash}.epub";
				$library->updateVolume($volume_id, $file, $_POST["title"], $_POST["volume"]);
				move_uploaded_file($_FILES["epub_file"]["tmp_name"], substr($file, 1));
			}
			else {
				$library->updateVolume($volume_id, $_POST["source"], $_POST["title"], $_POST["volume"]);
			}
			if ($_FILES["b_cover"]["size"] > 0 && $_FILES["b_cover"]["error"] == 0) {
				if (!is_dir("data/library/_covers")) mkdir("data/library/_covers");
				iTF($_FILES["b_cover"]["tmp_name"], "data/library/_covers/b_{$volume_id}.jpg", 1000, 94);
			}
			if ($_FILES["b_json"]["size"] > 0 && $_FILES["b_json"]["error"] == 0) {
				if (!is_dir("data/library/_json")) mkdir("data/library/_json");
				move_uploaded_file($_FILES["b_json"]["tmp_name"], "data/library/_json/{$volume_id}.txt");
			}
		}
		
		$volume_blob = $library->getVolume($volume_id);
		$partExists = "no!";
		if (file_exists("data/library/_json/{$volume_id}.txt")) $partExists = "YES!";
		echo "
			<form action='/app/LibraryManager?do=edit_volume&series_id={$series_id}&book_id={$volume_id}' method='post' enctype='multipart/form-data'>
				<input type='text' name='title' value=\"{$volume_blob['title']}\" />
				<input type='text' name='volume' value=\"{$volume_blob['volume']}\" />
				<input type='text' name='source' value=\"{$volume_blob['source']}\" />
				Volume Cover: <img src='/data/library/_covers/b_{$volume_id}.jpg' style='height: 320px;' /><br/><input type='file' name='b_cover' /><br />
				JSON txt exists: {$partExists}<br/><input type='file' name='b_json' /><br />
				ePub File: <input type='file' name='epub_file' />
				<input type='submit' value='Update Volume' />
			</form>
		";
	break;
}
else {
	foreach ($library->getAllSeries() as $blob) {
		echo "<a href='/app/LibraryManager?do=add_volumes&series_id={$blob['id']}'>{$blob['title']} - {$blob['author']}</a><br />";
	}
}
?>