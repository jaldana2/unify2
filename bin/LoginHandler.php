<?php
if (isset($_POST["loginUser"]) && isset($_POST["loginPass"])) {
	$username = $_POST["loginUser"];
	$password = $_POST["loginPass"];
	//$username = fnDecrypt(base64_decode($username));
	//$password = fnDecrypt(base64_decode($password));
	
	$auth = new Auth($mysqli);
	if ($auth->getUserPassword($username) == $auth->getNewPasswordHash($password)) {
		// password match (includes password-less accounts)
		$session = $auth->getUserSessionKey($username);
		setcookie("auth", $session, time() + (3600 * 24 * 30), "/");
		
		$dm = base64_decode($_GET["dm"]);
		if ($dm !== $_SERVER["HTTP_HOST"]) {
			if (isSomething($_GET["return"])) {
				header("Location: https://{$dm}/do/Saber?qs=" . $_GET["return"] . "&_s=" . base64_encode(fnEncrypt($session)));
			}
			else {
				header("Location: https://{$dm}/app/Home?welcome");
			}
		}
		else {
			if (isSomething($_GET["return"])) {
				header("Location: /app/Passthru?qs=" . $_GET["return"]);
			}
			else {
				header("Location: /app/Home?welcome");
			}
		}
	}
	else {
		if (isSomething($_GET["return"])) {
			header("Location: /app/Login?username={$username}&qs={$_GET['return']}&error=" . AUTH_ERROR_CRYPT_INVALID_ERROR . "&dm={$_GET['dm']}#login");
		}
		else {
			header("Location: /app/Login?username={$username}&error=" . AUTH_ERROR_CRYPT_INVALID_ERROR . "&dm={$_GET['dm']}#login");
		}
	}
}
elseif (isset($_GET["logout"])) {
	setcookie("auth", "", time() - 3600, "/");
	//session_start();
	//session_destroy();
	//print_a($_COOKIE);
	header("Location: /app/Home?goodbye");
}
?>