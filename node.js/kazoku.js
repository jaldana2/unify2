/* kazoku (swatch) */
var port = 8182;
var https = require('https');
var fs = require('fs');
var privateKey = fs.readFileSync('/etc/letsencrypt/live/aftermirror.com/privkey.pem');
var certificate = fs.readFileSync('/etc/letsencrypt/live/aftermirror.com/cert.pem');
var ca = fs.readFileSync('/etc/letsencrypt/live/aftermirror.com/chain.pem');
var sio = require('socket.io');
var express = require('express');
var credentials = {key:privateKey,cert:certificate,ca:ca};

var app = express();
app.set('port_https', port);

var httpsServer = https.createServer(credentials, app).listen(port);

var io = sio.listen(httpsServer, credentials);

console.log("kazoku server 0.1b1 @ port " + port);

var roomprop = {};
var rooms = {};
var users = {};

io.on('connection', function(socket) {
	console.log('A new user connected');
	var client_username;
	var client_room;
	
	socket.on('client identify', function(e) {
		console.log('new user ' + e.username + ' @ room ' + e.room);
		client_username = e.username;
		client_room = e.room;
		
		users[client_username] = client_room;
		if (typeof rooms[client_room] == 'undefined') {
			rooms[client_room] = {};
			rooms[client_room][client_username] = { entry: Date.now(), ping: -1, role: 'host' };
		}
		else {
			if (typeof rooms[client_room][client_username] == 'undefined') {
				rooms[client_room][client_username] = { entry: Date.now(), ping: -1, role: 'guest' };
			}
			else {
				rooms[client_room][client_username]['entry'] = Date.now();
			}
		}
		
		if (typeof roomprop[client_room] == 'undefined') {
			roomprop[client_room] = { status: 'idle', media: {}, position: 0, title: '', episode: '' };
		}
		
		socket.emit('connection ok');
		socket.join(client_room);
		io.to(client_room).emit('recv chat message', { username: client_username, message: '[joined the room]' });
		
		// check if room media is already set
		
		if (roomprop[client_room].status !== 'idle') {
			// broadcast media only to requested client.
			socket.emit('ready media', roomprop[client_room]);
		}
	});
	
	socket.on('request ping', function(startTime, prev_latency, cb) {
		rooms[client_room][client_username]['ping'] = prev_latency;
		cb(startTime);
	});
	socket.on('request room stat', function() {
		socket.emit('recv room stat', rooms[client_room]);
	});
	
	socket.on('send chat message', function(data) {
		io.to(client_room).emit('recv chat message', { username: client_username, message: data.message });
	});
	
	socket.on('set room media', function(data) {
		roomprop[client_room] = { status: 'ready', media: data.src, position: 0, title: data.title, episode: data.episode };
		io.to(client_room).emit('ready media', roomprop[client_room]);
	});
	
	socket.on('req media control', function(action) {
		io.to(client_room).emit('rcv media control', action);
	});
	
	socket.on('player status', function(data) {
		if (rooms[client_room][client_username].role == 'host') {
			roomprop[client_room].status = data.playState;
			roomprop[client_room].position = data.position;
		}
		var offset = data.position - roomprop[client_room].position;
		io.to(client_room).emit('player status', { user: client_username, offset: offset });
	});
	
	socket.on('disconnect', function() {
		io.to(client_room).emit('recv chat message', { username: client_username, message: '[left the room]' });
	})
});