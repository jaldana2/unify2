var fs = require('fs');
var https = require('https');
var express = require('express');
var socketio = require('socket.io');
/*
var sslOptions = {
    key: fs.readFileSync('/etc/ssl/aftermirror.key'),
    cert: fs.readFileSync('/etc/ssl/aftermirror-nokey.crt'),
    ca: fs.readFileSync('/etc/ssl/alphassl.crt'),
    ciphers: 'ECDHE-RSA-AES256-SHA:AES256-SHA:RC4-SHA:RC4:HIGH:!MD5:!aNULL:!EDH:!AESGCM',
    honorCipherOrder: true
};
*/

var app = express();
var server = require('http').Server(app);
var io = socketio.listen(server, {
    "log level" : 3,
    "match origin protocol" : true,
    "transports" : ['websocket', 'polling']
});
server.listen(182, function() {
	console.log('listening on *:182');
});

/*
var fs = require('fs');
var https = require('https');
var express = require('express');
var socketio = require('socket.io');
var sslOptions = {
    key: fs.readFileSync('/etc/ssl/aftermirror.key'),
    cert: fs.readFileSync('/etc/ssl/aftermirror-nokey.crt'),
    ca: fs.readFileSync('/etc/ssl/alphassl.crt'),
    ciphers: 'ECDHE-RSA-AES256-SHA:AES256-SHA:RC4-SHA:RC4:HIGH:!MD5:!aNULL:!EDH:!AESGCM',
    honorCipherOrder: true
};

var app = express();
var server = https.createServer(sslOptions, app);
var io = socketio.listen(server, {
    "log level" : 3,
    "match origin protocol" : true,
    "transports" : ['websocket', 'polling']
});
server.listen(182); 
*/

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

/* Swatch */
var clients = [];
var userlist = [];
var pingStats = [];
console.log("theatre server 0.1");

var swatch = io.of('/poke').on('connection', function(socket) {
	// Announce
	socket.on('user connect', function(e) {
		// emit new user back to ALL clients
		console.log("new user - " + e.username);
		swatch.emit("new user", e);
		pingStats[e.username] = -1;
	});
	socket.on('disconnect', function(e) {
		// eh. oh wells
	});
	
	// Player
	socket.on("interact", function(e) {
		swatch.emit("interact", e);
	});
	socket.on("status", function(e) {
		swatch.emit("status", e);
	});
	
	// Chat
	socket.on('chat message', function(msg) {
		if (msg.skipfilter) {
			swatch.emit('chat message', { message: msg.message, username: msg.username, room: msg.room });
		}
		else {
			swatch.emit('chat message', { message: htmlEntities(msg.message), username: msg.username, room: msg.room });
		}
	});
	
	// Ping
	socket.on('pong', function(e) {
		latency = Date.now() - e.start;
		pingStats[e.username] = latency;
		swatch.emit('latency', { username: e.username, room: e.room, latency: latency });
	});
});

setInterval(function() {
	var startTime = Date.now();
	swatch.emit('ping', { start: startTime });
}, 1000);
