<?php
include_once("config/sql.php");
include_once "engine.php";
$app = isset($_GET["app"]) ? (file_exists("bin/{$_GET['app']}.php") ? $_GET["app"] : "Home") : "Home";
if ($_SERVER["HTTP_HOST"] == "urusai.ninja" && $app == "Home" && !isset($_GET["goodbye"])) {
	$app = "Urusai";
}

if (isset($_GET["noUI"])) {
	include("bin/{$app}.php");
}
else {
	$gitStatus = new gitStatus();
	$_gitHeadStatus = $gitStatus->getGitCommitHead();
	include("config/subtitle.php");
?>
<!DOCTYPE HTML>
<!--
	Solid State by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>after|mirror</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link href="data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwPwAAwA8AAIAHAAD4AwAA/gMAAP4BAAD/AQAA/wEAAP8BAAD/AQAA/gEAAP4DAAD4AwAAgAcAAMAPAADwPwAA" rel="icon" type="image/x-icon" />
		<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
		<!--[if lte IE 8]><script src="/assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="/assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="/assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="/assets/css/ie8.css" /><![endif]-->
		
		<link href="/assets/css/aftermirror.css?v=<?php echo $_gitHeadStatus; ?>" rel="stylesheet">
		<link href="/assets/css/featherlight.min.css" rel="stylesheet">
		<link href="/assets/css/jquery-labelauty.css" rel="stylesheet">
		<link href="/assets/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
		<link href='https://cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.2/basic/jquery.qtip.min.css' rel='stylesheet' type='text/css'>
		<link href="/assets/css/spinkit.css" rel="stylesheet">
		<link href="/assets/css/select2.min.css" rel="stylesheet">
		<link href="/assets/css/jquery-sakura.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Fredoka+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Gentium+Book+Basic' rel='stylesheet'>
	</head>
	<body>

		<!-- Page Wrapper -->
			<div id="page-wrapper">

				<!-- Header -->
					<header id="header"<?php if ($app == "Home") { echo ' class="alt"'; } ?>>
						<h1><a href="/app/Home" class="am">after|mirror</a></h1>
						<nav>
							<?php
								if (defined("AUTH_USER")) {
									$notify = new Notify($mysqli);
									$my_notify = $notify->getNotificationCount(AUTH_UID);
									if ($my_notify >= 1) {
										printf("<a class=\"noline\" href=\"/app/Notify\" style='color: white; background-color: rgba(255, 0, 0, 0.5); border-radius: 4px; '><i class=\"fa fa-bell\"></i> %d</a>", $my_notify);
									}
									else {
										echo "<a class=\"noline\" href=\"/app/Notify\"><i class=\"fa fa-bell\"></i></a>";
									}
								}
							?>
							<a href="#menu">Menu</a>
						</nav>
					</header>

				<!-- Menu -->
					<nav id="menu">
						<div class="inner">
							<h2>Where to?</h2>
							<ul class="links">
								<li><a href="/">Home</a></li>
								<li><a href="/app/UrusaiLatte">Anime</a></li>
								<li><a href="/app/Tokyo">Music</a></li>
								<li><a href="/app/Gallery">Gallery</a></li>
								<li><a href="/app/Mango">Manga</a></li>
								<li><a href="/app/Library">Library</a></li>
								<li><a href="/app/Atelier">Atelier</a></li>
								<?php
									if (defined("AUTH_USER")) {
										?>
										<li><a href="/app/Mail">Mail</a></li>
										<li><a href="/app/Account"><?php echo AUTH_USER; ?></a></li>
										<li><a href="/do/LoginHandler?logout">Log Out</a></li>
										<?php
									}
									else {
										?>
										<li><a href="/app/Login">Log In</a></li>
										<li><a href="/app/Register?do=register">Sign Up</a></li>
										<?php
									}
								?>
							</ul>
							<a href="#" class="close">Close</a>
						</div>
					</nav>

				<?php
					if ($app == "Home") {
						?>
						<!-- Banner -->
						<section id="banner">
							<div class="inner">
								<div class="logo"><span class="icon fa-moon-o"></span></div>
								<h2 class="am">after|mirror</h2>
								<?php
									if (defined("AUTH_USER")) {
										printf("<p>Welcome back, %s!</p>", AUTH_USER);
									}
									else {
										echo "<p>Because it looks really pretty.</p>";
									}
								?>
							</div>
						</section>
						
						<!-- Wrapper -->
						<section id="wrapper">
				
						<!-- Content -->
							<div id="main-content">
								<div class="main-container-ajax">
									<?php
										include("bin/{$app}.php");
									?>
								</div>
							</div>
							
						</section>
						<?php
					}
					else {
						?>
						<!-- Wrapper -->
						<section id="wrapper">
							<header>
								<div class="inner">
									<h2><a href='/app/<?php echo $app; ?>'><?php if (isset($__title_rw[$app])) echo $__title_rw[$app]; else echo $app; ?></a></h2>
									<p><?php if (isset($__subtitle[$app])) echo $__subtitle[$app]; else echo $__subtitle["default"]; ?></p>
								</div>
							</header>
				
						<!-- Content -->
							<div class="wrapper" id="main-content">
								<div class="inner main-container-ajax">
									<?php
										include("bin/{$app}.php");
									?>
								</div>
							</div>
						</section>
						<?php
					}
				?>
			</div>

		<!-- Footer -->
			<section id="footer">
				<div class="inner">
					<h2 class="major">About this site</h2>
					<p>Hey there, welcome! Hope you enjoy whatever randomness you may stumble upon while you are here.</p>
					<ul class="contact">
						<li class="fa-envelope"><a href="mailto:admin@aftermirror.com">admin@aftermirror.com</a></li>
						<li class="fa-bitbucket"><a href="https://bitbucket.org/jaldana2/unify2">bitbucket (source code)</a></li>
						<li class="fa-heartbeat"><a href="/app/Uptime">uptime stats</a></li>
						<li class="fa-heart"><a href="/app/Credits">site credits</a></li>
					</ul>
					<ul class="copyright">
						<li>Made with <i class="fa fa-heart"></i> &mdash; <a href="/app/Home">after|mirror</a> <?php echo $gitStatus->getVersion() . "-" . $gitStatus->getGitCommitHead() . " (" . $gitStatus->lastModified() . " ago)"; ?></li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</div>
			</section>

		</div>
			
			
		<div id="page-transition-container">
			<div class="page-transition">
				<div class="sk-folding-cube">
					<div class="sk-cube1 sk-cube"></div>
					<div class="sk-cube2 sk-cube"></div>
					<div class="sk-cube4 sk-cube"></div>
					<div class="sk-cube3 sk-cube"></div>
				</div>
			</div>
		</div>

		
		
		<script>
		<?php
			echo "var x_git_revision = \"{$_gitHeadStatus}\";";
			if (defined("AUTH_USER")) {
				echo "var x_auth_user = \"" . AUTH_USER . "\";";
			}
			else {
				echo "var x_auth_user = \"guest\";";
			}
		?>
		</script>
		
		<!-- Scripts -->
		<script src="/assets/js/skel.min.js"></script>
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/jquery.scrollex.min.js"></script>
		<script src="/assets/js/util.js"></script>
		<!--[if lte IE 8]><script src="/assets/js/ie/respond.min.js"></script><![endif]-->
		<script src="/assets/js/main.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/bootstrap3-wysihtml5.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/featherlight/1.4.0/featherlight.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.1/js.cookie.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/blazy/1.5.4/blazy.min.js"></script>
		<script src="/assets/js/jquery-labelauty.js"></script>
		<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.2/basic/jquery.qtip.min.js"></script>-->
		<!--<script src="https://cdn.rawgit.com/nnattawat/flip/v1.0.20/dist/jquery.flip.min.js"></script>-->
		<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.3/Chart.min.js"></script>-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.5/socket.io.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.0/notify.min.js"></script>
		<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/stupidtable/0.0.1/stupidtable.min.js"></script>-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.10/clipboard.min.js"></script>
		<!--<script src="/assets/js/jquery.filtertable.min.js"></script>-->
		<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.25.8/js/jquery.tablesorter.min.js"></script>-->
		<script src="/assets/js/jquery.autogrowtextarea.min.js"></script>
		<script src="/assets/js/afterglow.min.js"></script>
		<script src="/assets/js/jquery-sakura.min.js"></script>
		<script src="/assets/js/navigator.js?v=<?php echo $_gitHeadStatus; ?>"></script>
		<script src="/assets/js/ui.js?v=<?php echo $_gitHeadStatus; ?>"></script>
		
		<?php
			if (file_exists("bin.js/{$app}.js")) {
				printf("<script src=\"%s\"></script>\n", "/bin.js/{$app}.js?v={$_gitHeadStatus}");
			}
		?>
	</body>
</html>
<?php
}
$mysqli->close();
?>