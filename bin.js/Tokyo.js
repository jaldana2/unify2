$(".btn-tab").on("click", function() {
	// hide all
	var controlTab = $(this).attr("data-tab");
	$(".btn-tab").each(function() {
		var loopTab = $(this).attr("data-tab");
		if (loopTab == controlTab) {
			$("#" + loopTab).fadeIn(200);
		}
		else {
			$("#" + loopTab).hide();
		}
	});
});
$("#upload-url").on("change", function() {
	// verify url
	// e.g. https://www.youtube.com/watch?v=CstgfAwBMNU
	
	var validYTCode = true;
	var YTCode;
	var YTman = false;
	if ($(this).val().length == 11) {
		// just the YouTube code length
		YTCode = $(this).val();
		if (YTCode.substring(0, 1) == "=") {
			YTman = true;
		}
	}
	else if ($(this).val().length >= 43) {
		// youtube url
		if ($(this).val().substring(0, 32) == "https://www.youtube.com/watch?v=") {
			YTCode = $(this).val().substring(32, 43);
		}
		else {
			alert("Invalid URL. It should look like https://www.youtube.com/watch?v=...");
			validYTCode = false;
		}
	}
	else {
		validYTCode = false;
	}
	if (validYTCode) {
		if (YTman) {
			console.log("Manual add");
			$("#upload-ctl").show();
			$("#upload-id").attr("value", YTCode);
		}
		else {
			console.log(YTCode);
			$("#upload-ctl").show();
			$("#upload-thumb").attr("src", "//i.ytimg.com/vi/" + YTCode + "/0.jpg");
			$("#upload-id").attr("value", YTCode);
		}
	}
	else {
		$("#upload-ctl").hide();
	}
});
$("#upload-btn").on("click", function(e) {
	var frmData = $("#upload-frm").serialize();
	$.notify("Uploading...", { position: "bottom right", className: "info" });
	$.post("/do/TokyoAjax", frmData).done(function(data) {
		$.notify(data, { position: "bottom right", className: "info" });
	});
	e.preventDefault();
	return false;
});
$(".tokyo-media").on("click", function() {
	if (activeId == $(this).attr("data-media-id") && !mediaPlayer.paused) {
		mediaPlayer.pause();
	}
	else {
		playMedia($(this).attr("data-media-id"));
	}
});
function str_pad_left(string, pad, length) {
	return (new Array(length+1).join(pad)+string).slice(-length);
}
function formatTime(value) {
	var time = Math.floor(value);
	var minutes = Math.floor(time / 60);
	var seconds = time - (minutes * 60);
	var ctime = minutes + ':' + str_pad_left(seconds,'0',2);
	return ctime;
}
function playMedia(id) {
	resetPlayButtons();
	console.log("play: " + id);
	activeId = id;
	location.hash = id;
	$("#mediaPlayer").attr("src", "/data/tokyo/" + id + ".mp3");
	//$("#aAlbumArt").css("background-image", "url(//i.ytimg.com/vi/" + id + "/0.jpg");
	$("#aAlbumArt").css("background-image", "url(" + $(".tokyo-media[data-media-id='" + id + "']").attr("data-thumb") + ")");
	mediaPlayer.load();
	mediaPlayer.play();
	$.post("/do/TokyoAjax", { playSong: id });
}
function updateSeeker() {
	if (mediaPlayer.paused) {
		$("#mPlayPause").removeClass("fa-pause").addClass("fa-play");
	}
	else {
		$("#mPlayPause").removeClass("fa-play").addClass("fa-pause");
	}
	if (!seekerMouseDown) {
		$("#mSeeker").prop("max", mediaPlayer.duration).val(mediaPlayer.currentTime);
	}
	$("#tPosition").text(formatTime(mediaPlayer.currentTime));
}
function setPlayButton() {
	// play
	if (activeId == "") return;
	$(".tokyo-media[data-media-id='" + activeId + "'] .tokyo-play-icon").addClass("fa-pause").removeClass("fa-play");
	document.title = $(".tokyo-media[data-media-id='" + activeId + "']").attr("data-title") + " - " + $(".tokyo-media[data-media-id='" + activeId + "']").attr("data-artist");
	$("#aPlayText").html("<b>" + $(".tokyo-media[data-media-id='" + activeId + "']").attr("data-title") + "</b> &mdash; " + $(".tokyo-media[data-media-id='" + activeId + "']").attr("data-artist"));
	$("#aAlbumArt").show();
	$("#aPlayText-container").show();
}
function resetPlayButtons() {
	// pause
	$(".fa-pause").removeClass("fa-pause").addClass("fa-play");
	document.title = "tokyo";
	$("#aAlbumArt").hide();
	$("#aPlayText-container").hide();
}
function playNextMedia() {
	if (activeId == "" || activeId == undefined) return;
	var next = activeId;
	if ($('#optRepeat').is(':checked')) {
		// do nothing, repeat.
	}
	else if ($('#optShuffle').is(':checked') && $("#tokyo-listing tr.tokyo-visible").length > 1) {
		do {
			next = $("#tokyo-listing tr.tokyo-visible").random().attr("data-media-id");
		}
		while (next == activeId); // so it doesn't play same song.
	}
	else {
		next = $(".tokyo-media[data-media-id='" + activeId + "']").nextAll("#tokyo-listing tr.tokyo-visible").first().attr("data-media-id");
	}
	if (next == "" || next == undefined) return; // lazy catch for end.
	playMedia(next);
}
function finishLoad() {
	if (newQueue !== "") {
		playMedia(newQueue);
		newQueue = "";
	}
	refreshList();
	updateFilter();
}
function refreshList() {
	$("#tokyo-listing").tablesorter();
	$("#tokyo-listing").filterTable({ label: "", containerTag: "div", containerClass: "form-group", placeholder: "Search...", visibleClass: "tokyo-visible" });
	$("input[type=search]").addClass("form-control");
}

$("#mVol").on("input", function() {
	mediaPlayer.volume = $(this).val();
});
$("#mSeeker").on("change", function() {
	mediaPlayer.pause();
	mediaPlayer.currentTime = $(this).val();
	mediaPlayer.play();
});
$("#mSeeker").on("mousedown", function() {
	seekerMouseDown = true;
});
$("#mSeeker").on("mouseup", function() {
	seekerMouseDown = false;
});
$("#mPlayPause").click(function() {
	if (mediaPlayer.paused) {
		mediaPlayer.play();
	}
	else {
		mediaPlayer.pause();
	}
});
$("#mediaPlayer").on("ended", function() {
	playNextMedia();
});
$("#hForward").click(function() {
	playNextMedia();
});
$("#sRepeat").click(function() {
	$("#optRepeat").click();
});
$("#sShuffle").click(function() {
	$("#optShuffle").click();
});
$("#optRepeat").click(function() {
	if ($(this).is(":checked")) {
		$("#sRepeat").css("color", "#006687");
	}
	else {
		$("#sRepeat").css("color", "#ffffff");
	}
});
$("#optShuffle").click(function() {
	if ($(this).is(":checked")) {
		$("#sShuffle").css("color", "#006687");
	}
	else {
		$("#sShuffle").css("color", "#ffffff");
	}
});

$(".tokyo-artist-toggle").on("click", function() {
	/*
	$(".tokyo-artist-toggle").each(function() {
		if ($(this).is(":checked")) {
			$(".tokyo-media[data-artist=\"" + $(this).attr("data-artist") + "\"]").show();
		}
		else {
			$(".tokyo-media[data-artist=\"" + $(this).attr("data-artist") + "\"]").hide();
		}
	});
	*/
	updateFilter();
});
$(".tokyo-tag-toggle").on("click", function() {
	/*
	$(".tokyo-tag-toggle").each(function() {
		if ($(this).is(":checked")) {
			$(".tokyo-media[data-artist=\"" + $(this).attr("data-artist") + "\"]").show();
		}
		else {
			$(".tokyo-media[data-artist=\"" + $(this).attr("data-artist") + "\"]").hide();
		}
	});
	*/
	updateFilter();
});
function updateFilter() {
	activeCount = 0;
	totalCount = 0;
	$(".tokyo-media").each(function() {
		$(this).removeClass("tokyo-visible");
		var show = true;
		if (!$(".tokyo-artist-toggle[data-artist=\"" + $(this).attr("data-artist") + "\"]").is(":checked")) {
			show = false;
		}
		var tags = $(this).attr("data-tags").split(",");
		$.each(tags, function(k, v) {
			if (!$(".tokyo-tag-toggle[data-tag=\"" + v + "\"]").is(":checked")) {
				show = false;
			}
		});
		if (show) {
			$(this).show().addClass("tokyo-visible");
			activeCount++;
		}
		else {
			$(this).hide()
		}
		totalCount++;
	});
	updateCount();
}
function updateCount() {
	$(".tokyo-active-count").text(activeCount);
	$(".tokyo-total-count").text(totalCount);
}

var activeId;
var mediaPlayer = $("#mediaPlayer")[0];
var seekerMouseDown = false;
var newQueue = "";
var activeCount = 0;
var totalCount = 0;

$(function() {
	$("#upload-tags").select2({
		tags: true,
		placeholder: "Tags"
	});
	if (location.hash.length == 12) {
		newQueue = location.hash.substring(1);
	}
	bLazy = new Blazy();
	setInterval(updateSeeker, 750);
	updateSeeker();
	mediaPlayer.onplay = function() { setPlayButton() };
	mediaPlayer.onpause = function() { resetPlayButtons() };
	finishLoad(); // or after list update
});
$.fn.random = function() {
	return this.eq(Math.floor(Math.random() * this.length));
}

// ctrl-F hook
$(window).keydown(function(e){
	if ((e.ctrlKey || e.metaKey) && e.keyCode === 70) {
		$("input[type='search']").focus();
		e.preventDefault();
	}
});