navigatorAjax = false;

$("#settingsUsePageTransition").on("click", function() {
	if ($(this).is(":checked")) {
		Cookies.set("settingsUsePageTransition", 'yes', { expires: 365, path: '/' });
		$.notify("Page Transitions enabled.", { className: "info" });
	}
	else {
		Cookies.set("settingsUsePageTransition", 'no', { expires: 365, path: '/' });
		$.notify("Page Transitions disabled.", { className: "info" });
	}
});
$(function() {
	if (Cookies.get("settingsUsePageTransition") !== undefined) {
		if (Cookies.get("settingsUsePageTransition") == "yes") {
			$("#settingsUsePageTransition").prop("checked", true);
		}
		else {
			$("#settingsUsePageTransition").prop("checked", false);
		}
	}
});