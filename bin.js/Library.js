$(document).keydown(function(e) {
    switch(e.which) {
        case 37: // left
			if (typeof book !== 'undefined') {
				book.prevPage();
			}
        break;

        case 39: // right
			if (typeof book !== 'undefined') {
				book.nextPage();
			}
        break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});