function loadPage(num) {
	if (num > 0 && num <= length) {
		$('#mangaImage').attr('src', 'images/loading.gif').attr('src', files[num]);
		$('#currentNum').text(num);
		if (num == 1) {
			preloadImages(length);
		}
		else if (num == length) {
			markAsRead();
		}
	}
	else if (num <= 0) {
		current = 1;
	}
	else if (num > length) {
		current = length;
	}
}

/* mangaReader */
function omvKeyPressed(e) {
	var keyCode = 0;
	if (navigator.appName == "Microsoft Internet Explorer") {
		if (!e) {
			var e = window.event;
		}
		if (e.keyCode) {
			keyCode = e.keyCode;
			if ((keyCode == 37) || (keyCode == 39)) {
				window.event.keyCode = 0;
			}
		} else {
			keyCode = e.which;
		}
	} else {
		if (e.which) {
			keyCode = e.which;
		} else {
			keyCode = e.keyCode;
		}
	}
	switch (keyCode) {
		case 37:
			current--;
			loadPage(current);
			return false;
		break;
		case 39:
			current++;
			loadPage(current);
			return false;
		break;
		default:
			return true;
		break;
	}
}
document.onkeydown = omvKeyPressed;

function preloadImages(len) {
	for (var i = 2; i <= len; i++) {
		//$('<img />').attr('src', dir + i + '.jpg');
		$('<img />').attr('src', files[i]);
	}
}
function markAsRead() {
	if (!notificationSent) {
		$.post('/do/AjaxNotification?mangoTracker', { series: manga, chapter: chapter });
		notificationSent = true;
	}
}
$(function(e) {
	if (typeof manga !== 'undefined') {
		loadPage(current);
		$('html, body').animate({
			scrollTop: $('#mangaBox').offset().top
		}, 400);
	}
	$('.cpicker').change(function(e) {
		location.href = $(this).attr('base-url') + $(this).val();
	});
});